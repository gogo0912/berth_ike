from sys import exc_info
from traceback import format_exc

from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple
from jsonrpc import JSONRPCResponseManager, dispatcher

from ctbike import qcs


SVR_ADDR = ('localhost', 2303)

VERBOSE = True


def qcschedule(pattern, limit, cranes, hatches, covers=None, QCI=None, ctime_limit=10):
    (r, d), (pD, pL) = zip(*cranes), zip(*hatches)
    d = [min(di, limit) for di in d]
    
    try:
        R = qcs.solve(pattern, r, d, pD, pL, covers, QCI, ctime_limit, VERBOSE)
    except Exception:
        # A way of forwarding exception to client (TODO Better way?)
        raise Exception(format_exc()).with_traceback(exc_info()[2])
    
    if VERBOSE:
        for QCI, ms, S, optimal, _ in R:
            print('#' * 80)
            print('### QC %s with makespan %.1f' % (QCI, ms), '*' if optimal else '(--)')
            for i, Si in zip(QCI, S):
                for h, t, s, p in Si:
                    print('  C%d H%d(%s) %.1f -- %.1f (%.1f)' % (i, h, t, s, s + p, p))
    
    return [dict(cranes=QCI, pattern=pattern, complete=ms, queue=S, optimal=opt)
            for QCI, ms, S, opt, _ in R]


def test(pb_name):
    from ctbike.qcs import prob
    r, d, pD, pL = getattr(prob, pb_name)()
    return qcschedule('hatch-wise', max(d), [*zip(r, d)], [*zip(pD, pL)])


def run():
    dispatcher['qcschedule'] = qcschedule
    dispatcher['test'] = test
    
    @Request.application
    def application(request):
        response = JSONRPCResponseManager.handle(request.data, dispatcher)
        return Response(response.json, mimetype='application/json')
    
    run_simple(*SVR_ADDR, application)


if __name__ == '__main__':
    run()
