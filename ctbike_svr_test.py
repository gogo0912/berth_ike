"""
Test ctbike_svr
"""

from collections import Sequence

import json
import requests


URL = 'http://localhost:2303/jsonrpc'
HEADERS = {'content-type': 'application/json'}


def test_test():
    R = requests.post(URL,
                      data=json.dumps(dict(method='test',
                                           params=dict(pb_name='ex_clt1'),
                                           jsonrpc=2.0, id=0)),
                      headers=HEADERS).json()
    print(R)


def test_qcschedule():
    params = {
        'pattern' : 'Discharge-first',
        'limit'   : 132500,
        'cranes'  : [[0, 132500], [0, 132500], [0, 132500]],
        #'cranes'  : [[0, 132500], [10, 0], [0, 132500]],
        'hatches' : [[2880,  6624], [ 4896,  9792], [9936, 15552], [3312, 18000],
                     [5184, 17568], [10800, 18576], [7632, 20160], [6912, 17856],
                     [8208, 15840], [ 3312, 18144], [6768, 15840], [ 144,  8928],
                     [ 432,  2160]],
        #'covers'  : [[0, 3], [0, 6], [2, 12]],
        #'QCI'     : [[0, 1, 2]],
        'ctime_limit' : 10
    }
    
    data = {
        'method'  :'qcschedule',
        'params'  : params,
        'jsonrpc' : 2.0,
        'id'      : 0
    }
    
    R = requests.post(URL, data=json.dumps(data), headers=HEADERS).json()
    
    print(R)
    
    if 'result' in R:
        for Rk in R['result']:
            print(Rk['cranes'], Rk['pattern'], Rk['complete'], Rk['optimal'])
            print(Rk['queue'])
    
    elif 'error' in R:
        # We handle the following error case only.
        e = R['error']
        assert 'code' in e and e['code'] == -32000 and 'data' in e, e
        ed = e['data']
        assert 'args' in ed and isinstance(ed['args'], Sequence) and ed['args'], e
        
        print('Exception at server!!!')
        print(ed['args'][-1])
    
    else:
        assert False, R


if __name__ == "__main__":
    #test_test()
    test_qcschedule()
