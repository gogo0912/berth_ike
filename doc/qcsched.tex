%\documentclass{article}
\documentclass[11pt,a4paper]{article} \usepackage{abstract} \renewcommand{\abstractnamefont}{\normalfont\Large\bfseries} \renewcommand{\abstracttextfont}{\normalfont\normalsize} \usepackage{setspace} \setstretch{1.8} \usepackage{layout} \addtolength{\textwidth}{4cm} \addtolength{\textheight}{2cm} \addtolength{\hoffset}{-2cm} \addtolength{\voffset}{-1.2cm}

\usepackage{kotex}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{multirow}

\usepackage{amsmath}\allowdisplaybreaks

\theoremstyle{definition}
\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}
\newtheorem{proposition}{Proposition}
\newtheorem{conjecture}{Conjecture}
\newtheorem{example}{Example}

% set notation, absolute value
\usepackage{mathtools}
\DeclarePairedDelimiter{\sete}{\{}{\}}
\DeclarePairedDelimiterX{\seti}[2]{\{}{\}}
  {#1 \mathrel{}\mathclose{}\delimsize|\mathopen{}\mathrel{} #2}
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\paren}{(}{)}  % parenthesis
\DeclarePairedDelimiter{\bracket}{[}{]}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}

\newcommand{\mc}{\mathcal}
\newcommand{\mr}{\mathrm}

\newcommand{\phat}{\hat{p}}


\title{Quay Crane Scheduling for a Vessel}
\author{Byung-Hyun Ha}


\begin{document}

\maketitle


\section{Problem Description}

A container vessel has \emph{hatches} (e.g., H1, H2, etc. in Fig.~\ref{fig:vessel}). There are a \emph{discharge job} and a \emph{loading job} for each hatch, which are processed by \emph{quay cranes (QCs)}. Suppose QCs and hatches are numbered from left to right. 
\begin{figure}
\centering \includegraphics[scale=0.8]{imgs/vessel.jpg}
\caption{A container vessel and hatches} \label{fig:vessel}
\end{figure}

In a hatch, the discharge job is processed first and the loading job is next. For a QC to handle more than one hatch, it conforms to one of the following work patterns: \emph{hatch-wise}, \emph{discharge-first}, and \emph{same-direction}. To illustrate the patterns, suppose a QC handles hatch~3, 4, and 5. Let $D_j$ and $L_j$ denote, respectively, the discharge job and the loading job in hatch~$j$. The handling sequences are:
\begin{description} \setlength\itemsep{-0.3em}
\item[Hatch-wise] The QC processes $D_3 \to L_3$, $D_4 \to L_4$, and $D_5 \to L_5$, in sequence.
\item[Discharge-first] The QC processes $D_3 \to D_4 \to D_5$ and $L_5 \to L_4 \to L_3$, in sequence.
\item[Same-direction] The QC processes $D_3 \to D_4 \to D_5$ and $L_3 \to L_4 \to L_5$, in sequence.
\end{description}

We consider:
\begin{itemize} \setlength\itemsep{-0.3em}
\item A job can be \emph{split} and processed by more than one QC, i.e., preemption is allowed.
\item QCs cannot cross each other.
\item Two QCs cannot handle adjacent hatches at the same time to avoid interference.
\item The hatch switch time (gantry travel and setup times) of a QC is negligible.
\item Some QCs may have the ready time and the deadline.
\item Some QCs can handle only part of hatches (hatch coverage) due to limited power cable length.
\end{itemize}

The goal is to determine the optimal job split and job-processing schedule by QCs that minimizes the makespan, i.e., the maximum completion time of all jobs. The input parameters are as follows:
\begin{itemize} \setlength\itemsep{-0.3em}
\item[$n$:] number of hatches
\item[$m$:] number of QCs
\item[$p_j^D$:] processing time of the discharge job in hatch~$j$ (which can be zero)
\item[$p_j^L$:] processing time of the loading job in hatch~$j$ (which can be zero)
\item[$r_i$:] QC~$i$의 투입 가능 시간(ready time)
\item[$d_i$:] QC~$i$가 작업을 마쳐야 하는 시간(deadline)
\item[$H_i^0$:] QC~$i$가 작업할 수 있는 시작 hatch
\item[$H_i^1$:] QC~$i$가 작업할 수 있는 끝 hatch
\end{itemize}

수리 모형 작성을 위해 다음의 정의를 사용한다.
\begin{itemize} \setlength\itemsep{-0.3em}
\item $r_{\min} = \min_i\sete{r_i}$: 작업 시작 시점(이 시간부터 계획을 수립)
\item $d_{\max} = \max_i\sete{d_i}$: 작업 종료 시점(이 시간까지만 계획을 수립)
\item $A = \seti*{i \in [1,n]}{r_i > r_{\min}}$\footnote{$[a, b] = \sete{a, a+1, \ldots, b}$를 의미함}: 작업 시작 시점 $r_{\min}$ 이후에 투입되는 QC들의 집합
\item $B = \seti*{i \in [1,n]}{d_i < d_{\max}}$: 작업 종료 시점 $d_{\max}$ 이전에 다른 곳에 보내져야 하는 QC들의 집합
\end{itemize}
본 문서에서는 논의를 단순화시키기 위해서 $r_{\min} = 0$으로 가정한다. 만일 0이 아니라면 모든 ready time과 deadline에 각각 $r_{\min}$을 빼고 계획을 수립한 후 결과에 $r_{\min}$을 더해주면 된다.

\section{Hatch-wise Pattern}

Hatch-wise pattern에서는 양적하 작업을 구분할 필요가 없으므로 간결한 표현을 위해 양적하 작업의 합한 값을 hatch~$j$의 작업량 $p_j$로 사용한다. 그리고 각 hatch~$j$에 대하여 QC~$i$가 최대로 할 수 있는 작업량을 $\phat_{ij}$로 정의한다. 즉,
\begin{align}
p_j &= p_j^D + p_j^L  \\
\phat_{ij} &= \min(d_i - r_i, p_j)
\end{align}

\subsection{초기해를 위한 MIP (mixed-integer programming) 모형}

\subsubsection{결정변수}

다음의 결정변수를 사용한다:
\begin{itemize} \setlength\itemsep{-0.3em}
\item[$x_{ij}$:] QC~$i$가 hatch~$(j-1)$에서 hatch~$j$로 이동하는 시각 ($i \in [1,m]$, $j \in [1,n+1]$)
  \begin{itemize} \setlength\itemsep{-0.3em}
  \item QC가 특정 hatch에서 머무르른 시간을 나타낸다. 그 시간 동안 그 hatch에서 작업이 가능하다. 구체적으로 QC~$i$가 hatch~$j$에서 머무르면서 작업이 가능한 시간은 $x_{ij}$부터 $x_{i,(j+1)}$까지이다. 
  \item Hatch~$(n+1)$은 QC가 hatch~$n$에서 머무르는 시간을 표현하기 위해 사용되는 가상의 hatch이다.
  \end{itemize}
\item[$y_{ij}$:] QC~$i$가 hatch~$j$에서 처리하는 작업량 ($i \in [1,m]$, $j \in [1,n]$)
\item[$u_{ij}$:] Ready time이 $r_{\min}$보다 큰 QC~$i$가 hatch~$j$에서 작업을 하면 0, 그렇지 않으면 1 ($i \in A$, $j \in [1,n]$)
\item[$v_{ij}$:] Deadline이 $d_{\max}$보다 작은 QC~$i$가 hatch~$j$에서 작업을 하면 1, 그렇지 않으면 0 ($i \in B$, $j \in [1,n]$)
\end{itemize}

각 결정변수 값의 유효 범위는 다음과 같다:
\begin{align}
0 \le x_{ij} \le d_{\max}  &\qquad \forall i \in [1,m], \ \forall j \in [1,n+1]  \label{eq:hw_x_domain}  \\
0 \le y_{ij} \le \phat_{ij}  &\qquad \forall i \in [1,m], \ \forall j \in [1,n]  \label{eq:hw_y_domain}  \\
u_{ij} \in \sete{0, 1}  &\qquad \forall i \in A, \ \forall j \in [1,n]  \\
v_{ij} \in \sete{0, 1}  &\qquad \forall i \in B, \ \forall j \in [1,n]
\end{align}

\subsubsection{목적함수}

모든 QC가 작업을 완료하는 시각은 제일 왼쪽 QC가 제일 마지막 hatch 작업을 끝내는 시각이다. 이는 아래 Eq.~(\ref{eq:hw_interfere})에 의해 유효하다. 따라서 목적함수(objective function) $z$는 다음과 같다:
\begin{align}
\mbox{minimize \ } z = x_{1,n+1}  \label{eq:hw_obj}
\end{align}

\subsubsection{제약식}

가능한 일정을 도출하는 제약식은 다음과 같다. 먼저 각 hatch에 머무르는 시간 이상으로 작업을 처리할 수 없다:
\begin{align}
y_{ij} \le x_{i,j+1} - x_{ij}  \qquad \forall i \in [1,m], \ \forall j \in [1,n]
\end{align}
각 QC가 작업한 총 시간의 합은 작업 처리시간($p_j$)보다 커야 한다:
\begin{align}
\sum_{i=1}^m y_{ij} \ge p_j  \qquad \forall j \in [1,n]  \label{eq:hw_sum_proc}
\end{align}
어떤 QC~$i$가 어떤 hatch~$j$로 들어가기 위해서는 오른쪽에 있는 QC~$(i+1)$가 한 칸 떨어진 hatch~$(j+2)$로 나가야 한다(즉, QC는 인접한 hatch를 동시에 작업할 수 없다):
\begin{align}
x_{ij} \ge x_{i+1,j+2}  \qquad \forall i \in [1,m-1], \ \forall j \in [1,n-1]  \label{eq:hw_interfere}
\end{align}

다음은 ready time과 deadline을 고려하여 작업을 못하는 경우($u_{ij} = 1$ 또는 $v_{ij} = 0$)를 제한한다(여기서 $M_{ij}$는 충분히 큰 수):
\begin{align}
y_{ij} \le M_{ij}(1 - u_{ij})  &\qquad \forall i \in A, \ \forall j \in [1,n]  \\
y_{ij} \le M_{ij} v_{ij}  &\qquad \forall i \in B, \ \forall j \in [1,n]
\end{align}
Ready time을 고려하여 작업이 가능한 경우라도($u_{ij} = 0$) ready time ($r_i$)부터 hatch를 떠나는 시점($x_{i,j+1}$)까지만 작업을 처리할 수 있다(여기서 $M^r_{ij}$는 충분히 큰 수):
\begin{align}
y_{ij} \le x_{i,j+1} - r_i + M^r_{ij} u_{ij}  \qquad \forall i \in A, \ \forall j \in [1,n]
\end{align}
유사하게 deadline을 고려하여 작업이 가능한 경우라도($v_{ij} = 1$) hatch에 들어오는 시점($x_{ij}$)부터 deadline ($d_i$)까지만 작업을 처리할 수 있다(여기서 $M^d_{ij}$는 충분히 큰 수):
\begin{align}
y_{ij} \le d_i - x_{ij} + M^d_{ij}(1 - v_{ij})  \qquad \forall i \in B, \ \forall j \in [1,n]
\end{align}
현재 각각의 충분히 큰 수는 다음과 같이 쉽게 구할 수 있는 값을 사용한다.
\begin{align}
M_{ij} &= \phat_{ij} \\
M^r_{ij} &= r_i \\
M^d_{ij} &= d_{\max} - d_i
\end{align}
만일 이들 보다 더 작은 값을 얻을 수 있다면 수리모형의 성능이 더 좋아질 수 있을 것으로 예상된다.

작업할 수 없는 해치에 대한 제약은 다음과 같다:
\begin{align}
y_{ij} = 0  \qquad \forall i \in [1,n], \ \forall j \in [1,H_i^0-1] \cup [H_i^1+1,n]  \label{eq:hw_range}
\end{align}

\subsubsection{Valid inequalities}

Valid inequality를 사용하여 수리모형의 성능을 높일 수 있다. 현재 비록 단순한 것들만 사용하고 있으나 MIPCL을 사용하여 풀이할 때 100배 이상의 성능 향상 효과를 보여준다. 먼저 ready time을 고려한 작업 가능성과 관련하여 다음을 고려할 수 있다:
\begin{align}
u_{ij} \ge u_{i,j+1}  &\qquad \forall i \in A, \ \forall j \in [1,n-1]  \\
x_{i,j+1} \le r_i + (d_{\max} - r_i)(1 - u_{ij})  &\qquad \forall i \in A, \ \forall j \in [1,n]
\end{align}
유사하게 deadline과 관련하여 다음을 고려할 수 있다:
\begin{align}
v_{ij} \ge v_{i,j+1}  &\qquad \forall i \in B, \ \forall j \in [1,n-1]  \\
x_{i,j} \le d_i + (d_{\max} - d_i)(1 - v_{ij})  &\qquad \forall i \in B, \ \forall j \in [1,n]
\end{align}

\subsection{해의 개선}  \label{sec:hw_improve}

MIP를 사용해 풀이한 초기해는 목적함수로 최소 완료시간만을 고려하므로 QC의 일정이 연속되지 않고 여러 hatch에 걸쳐있는 복잡한 형태로 도출되는 것이 일반적이다. 따라서 최소 완료시간을 늘리지 않는 한에서 더 나은 일정을 찾을 필요가 있다(\verb|qc_sched.ppt|의 슬라이드~19 참조). 이를 위해 동일한 최소 완료시간~$z^*$를 가지면서 동시에 설정된 작업범위($W_i^0$에서 $W_i^1$까지)를 만족하는 일정을 도출하는 선형계획(linear programming) [LP-HW]을 활용하여 지역 탐색(local search)를 수행한다.

지역 탐색에서는 MIP를 사용하여 도출한 초기해의 모든 QC~$i$에 대한 $(W_i^0, W_i^1)$로부터 시작하여 최소 완료시간~$z^*$를 늘리지 않으면서 더 나은 형태의 $(W_i^0, W_i^1)$을 가지는 이웃해를 탐색한다. 작업범위를 더 변경시킬 수 없을 경우 탐색을 종료한다. 구체적으로 절차는 다음과 같다:
\begin{enumerate}
\item Set $i_0 \gets 1$ and $i_1 \gets m$.
\item QC~$i_0$의 작업 시작 hatch ($W_{i_0}^0$)를 가능한 왼쪽으로 당긴다.
\item QC~$i_1$의 작업 끝 hatch ($W_{i_1}^1$)를 가능한 오른쪽으로 민다.
\item 다음을 반복한다.
  \begin{enumerate}
  \item QC~$i_0$의 작업 끝 hatch ($W_{i_0}^1$)를 가능한 왼쪽으로 당긴다.
  \item QC~$i_1$의 작업 시작 hatch ($W_{i_1}^0$)를 가능한 오른쪽으로 민다.
  \item Set $i_0 \gets i_0 + 1$ and $i_1 \gets i_1 - 1$. 만일 $i_0 \ge i_1$이면 종료한다.
  \item QC~$i_0$의 작업 시작 hatch ($W_{i_0}^0$)를 가능한 오른쪽으로 민다.
  \item QC~$i_1$의 작업 끝 hatch ($W_{i_1}^1$)를 가능한 왼쪽으로 당긴다.
  \end{enumerate}
\end{enumerate}
참고로 동일한 [LP-HW]를 사용하여 더 나은 형태의 일정을 구하는 방법을 개발할 수 있을 것이다.

선형계획 모형 [LP-HW]의 결정변수는 MIP의 결정변수들 중 $x_{ij}$와 $y_{ij}$를 사용한다. 이들의 유효범위는 MIP의 경우와 동일하다: Eqs.~(\ref{eq:hw_x_domain}) and (\ref{eq:hw_y_domain}). 목적식은 MIP의 Eq.~(\ref{eq:hw_obj})와 같다. (참고로 아래의 제약 Eq.~(\ref{eq:hw_opt_z})을 사용하므로 사용하지 않아도 무방하다. 대신 더 좋은 형태의 일정을 나타내는 점수를 개발하여 사용할 수 있을 것이다.)

[LP-HW]의 제약식은 다음의 MIP 제약식을 재사용한다: Eqs.~(\ref{eq:hw_sum_proc}), (\ref{eq:hw_interfere}), and (\ref{eq:hw_range}). 최소 완료시간의 제약은 다음과 같다:
\begin{align}
x_{1,n+1} \le z^*  \label{eq:hw_opt_z}
\end{align}
추가적으로 $(W_i^0, W_i^1)$과 관련된 다음의 제약식이 사용된다:
\begin{align}
y_{iW_i^0} \le x_{i,W_i^0+1} - r_i  &\qquad \forall i \in [1,m]  \\
y_{iW_i^1} \le d_i - x_{i,W_i^1}  &\qquad \forall i \in [1,m]  \\
y_{ij} <= x_{i,j+1} - x_{ij}  &\qquad \forall i \in [1,m], \ \forall j \in [W_i^0, W_i^1]  \\
y_{ij} = 0  &\qquad \forall i \in [1,m], \ \forall j \in [1,W_i^0-1] \cup [W_i^1+1,n]
\end{align}


\section{Discharge-first Pattern}

\subsection{초기해를 위한 MIP 모형}

Hatch-wise pattern과 유사하게 모형화를 위해 각 작업 형태에 대하여 QC가 hatch에서 최대로 할 수 있는 작업량을 정의한다:
\begin{align}
\phat^D_{ij} &= \min(d_i - r_i, p^D_j)  \\
\phat^L_{ij} &= \min(d_i - r_i, p^L_j)
\end{align}


\subsubsection{결정변수}

Discharge-first pattern에서는 양하와 작하 작업을 구분해야 한다. 따라서 hatch-wise pattern에서 같이 함께 묶어 사용했던 결정변수를 각각 구분하며, 그 의미는 동일하다:
\begin{itemize}
\item $x^D_{ij}$ and $x^L_{ij}$, for $i \in [1,m]$, for $j \in [1,n+1]$ (단, 여기서 $x^L_{ij}$는 적하 작업을 위해 QC~$i$가 hatch~$j$에서 hatch~$(j-1)$로 이동한 시간을 의미한다.)
\item $y^D_{ij}$ and $y^L_{ij}$, for $i \in [1,m]$, for $j \in [1,n]$
\item $u^D_{ij}$ and $u^L_{ij}$, for $i \in A$, for $j \in [1,n]$
\item $v^D_{ij}$ and $v^L_{ij}$, for $i \in B$, for $j \in [1,n]$
\end{itemize}

추가적으로 각 QC가 양하를 마친 후 적하 작업을 처리하는 것을 고려하기 위해 다음의 이진 결정변수를 사용한다:
\begin{itemize}
\item $w_{ij}$: QC~$i$가 hatch~$j$에서 작업을 하면 1, 그렇지 않으면 0 ($i \in [1,m]$, $j \in [1,n]$)
\end{itemize}

유효 범위는 다음과 같다(각 $t = D, L$에 대하여):
\begin{align}
0 \le x^t_{ij} \le d_{\max}  &\qquad \forall i \in [1,m], \ \forall j \in [1,n+1]  \label{eq:df_x_domain}  \\
0 \le y^t_{ij} \le \phat^t_{ij}  &\qquad \forall i \in [1,m], \ \forall j \in [1,n]  \label{eq:df_y_domain}  \\
u^t_{ij} \in \sete{0, 1}  &\qquad \forall i \in A, \ \forall j \in [1,n]  \\
v^t_{ij} \in \sete{0, 1}  &\qquad \forall i \in B, \ \forall j \in [1,n]  \\
w_{ij} \in \sete{0, 1}  &\qquad \forall i \in [1,m], \ \forall j \in [1,n]
\end{align}

\subsubsection{목적함수}

모든 QC가 작업을 완료하는 시각은 제일 오른쪽 QC가 제일 첫 hatch 작업을 끝내는 시각이다. 따라서 목적함수(objective function) $z$는 다음과 같다:
\begin{align}
\mbox{minimize \ } z = x_{m1}  \label{eq:df_obj}
\end{align}

\subsubsection{제약식}

제약식은 양하 작업과 적하 작업의 순서를 나타내는 것 외는 hatch-wise 경우와 유사하다. 다음은 머무르는 시간과 작업시간의 관계를 나타낸다:
\begin{align}
y^D_{ij} \le x^D_{i,j+1} - x^D_{ij}  &\qquad \forall i \in [1,m], \ \forall j \in [1,n]  \\
y^L_{ij} \le x^L_{ij} - x^L_{i,j+1}  &\qquad \forall i \in [1,m], \ \forall j \in [1,n]
\end{align}
다음은 작업 처리시간에 관한 것이다:
\begin{align}
\sum_{i=1}^m y^D_{ij} \ge p^D_j  &\qquad \forall j \in [1,n]  \label{eq:df_sum_proc}  \\
\sum_{i=1}^m y^L_{ij} \ge p^L_j  &\qquad \forall j \in [1,n]  \label{eq:df_sum_proc}
\end{align}
다음은 인접한 hatch에서의 작업을 피하는 것이다:
\begin{align}
x^D_{ij} \ge x^D_{i+1,j+2}  &\qquad \forall i \in [1,m-1], \ \forall j \in [1,n-1]  \label{eq:df_interfere_D}  \\
x^L_{ij} \le x^L_{i+1,j+2}  &\qquad \forall i \in [1,m-1], \ \forall j \in [1,n-1]  \label{eq:df_interfere_L}
\end{align}

다음은 ready time과 deadline을 고려하여 작업을 못하는 경우를 제한한다:
\begin{align}
y^D_{ij} \le \phat^D_{ij}(1 - u^D_{ij})  &\qquad \forall i \in A, \ \forall j \in [1,n]  \\
y^L_{ij} \le \phat^L_{ij}(1 - u^L_{ij})  &\qquad \forall i \in A, \ \forall j \in [1,n]  \\
y^D_{ij} \le \phat^D_{ij} v^D_{ij}  &\qquad \forall i \in B, \ \forall j \in [1,n]  \\
y^L_{ij} \le \phat^L_{ij} v^L_{ij}  &\qquad \forall i \in B, \ \forall j \in [1,n]
\end{align}
Ready time을 고려하여 작업이 가능한 경우라도 ready time부터 hatch를 떠나는 시점까지만 작업을 처리할 수 있다:
\begin{align}
y^D_{ij} \le x^D_{i,j+1} - r_i + r_i u^D_{ij}  &\qquad \forall i \in A, \ \forall j \in [1,n]  \\
y^L_{ij} \le x^L_{i,j} - r_i + r_i u^L_{ij}  &\qquad \forall i \in A, \ \forall j \in [1,n]
\end{align}
Deadline을 고려하는 경우는 다음과 같다:
\begin{align}
y^D_{ij} \le d_i - x^D_{ij} + (d_{\max} - d_i)(1 - v^D_{ij})  &\qquad \forall i \in B, \ \forall j \in [1,n]  \\
y^L_{ij} \le d_i - x^L_{i,j+1} + (d_{\max} - d_i)(1 - v^L_{ij})  &\qquad \forall i \in B, \ \forall j \in [1,n]
\end{align}

작업할 수 없는 해치에 대한 제약은 다음과 같다:
\begin{align}
y^D_{ij} = y^L_{ij} = 0  \qquad \forall i \in [1,n], \ \forall j \in [1,H_i^0-1] \cup [H_i^1+1,n]  \label{eq:hw_range}
\end{align}

양하 작업과 적하 작업의 순서 유지를 위한 제약식은 다음과 같다:
\begin{align}
y^D_{ij} + y^L_{ij} \le x^L_{ij} - x^D_{ij} + d_{\max}(1 - w_{ij})  &\qquad \forall i \in [1,m], \ \forall j \in [1,n]  \\
y^D_{ij} + y^L_{ij} \le \min(\phat^D_{ij} + \phat^L{ij}, d_i - r_i)  &\qquad \forall i \in [1,m], \ \forall j \in [1,n]  \\
y^D_{ij} \le \phat^D_{ij} w_{ij}  &\qquad \forall i \in [1,m], \ \forall j \in [1,n]  \\
y^L_{ij} \le \phat^L_{ij} w_{ij}  &\qquad \forall i \in [1,m], \ \forall j \in [1,n]  \\
y^D_{ij} + y^L_{ij} \le x^L_{ij} - r_i + r_i u^L_{ij}  &\qquad \forall i \in A, \ \forall j \in [1,n]  \\
y^D_{ij} + y^L_{ij} \le d_i - x^D_{ij} + (d_{\max} - d_i) (1 - v^D_{ij})  &\qquad \forall i \in B, \ \forall j \in [1,n]  \\
y^D_{ij} \le \phat^D_{ij} (1 - u^L_{ij})  &\qquad \forall i \in A, \ \forall j \in [1,n]  \\
y^L_{ij} \le \phat^L_{ij} v^D_{ij}  &\qquad \forall i \in B, \ \forall j \in [1,n]
\end{align}


\subsubsection{Valid inequalities}

현재 다음을 사용한다:
\begin{align}
u^D_{ij} \ge u^D_{i,j+1}  &\qquad \forall i \in A, \ \forall j \in [1,n-1]  \\
u^L_{i,j+1} \ge u^L_{ij}  &\qquad \forall i \in A, \ \forall j \in [1,n-1]  \\
x^D_{i,j+1} \le r_i + (d_{\max} - r_i)(1 - u^D_{ij})  &\qquad \forall i \in A, \ \forall j \in [1,n]  \\
x^L_{ij} \le r_i + (d_{\max} - r_i) (1 - u^L_{ij})  &\qquad \forall i \in A, \ \forall j \in [1,n]  \\
v^D_{ij} \ge v^D_{i,j+1}  &\qquad \forall i \in B, \ \forall j \in [1,n-1]  \\
v^L_{i,j+1} \ge v^L_{ij}  &\qquad \forall i \in B, \ \forall j \in [1,n-1]  \\
x^D_{i,j} \le d_i + (d_{\max} - d_i)(1 - v^D_{ij})  &\qquad \forall i \in B, \ \forall j \in [1,n]  \\
x^L_{i,j+1} \le d_i + (d_{\max} - d_i) (1 - v^L_{ij})  &\qquad \forall i \in B, \ \forall j \in [1,n]
\end{align}
추가로 다음을 사용한다:
\begin{align}
w_{ij} \ge w_{i,j+1}  &\qquad \forall i \in A, \ \forall j \in [1,n-1]  \\
w_{i+1,j+2} \ge w_{ij}  &\qquad \forall i \in A, \ \forall j \in [1,n-2]
\end{align}


\subsection{해의 개선}

Discharge-first pattern의 해를 개선하기 위해서 hatch-wise의 방식(Sec.~\ref{sec:hw_improve})을 양하 작업 일정과 적하 작업 일정에 각각 적용한다. 이웃해 탐색을 위해 역시 유사한 선형계획을 사용한다.


\end{document}
