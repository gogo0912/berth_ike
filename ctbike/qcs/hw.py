"""
Hatch-wise with yet another approach
"""

import numpy as np

from mipcl_py.mipshell.mipshell import Problem, minimize, Var, sum_, REAL, BIN

from ctbike.qcs import prob
EPSILON = prob.EPSILON 


PRINT_MIP_FORMULATION = 0
PRINT_LP_FORMULATION = 0


def solve(r, d, pD, pL, H=None, ctime_limit=10, use_VC=True, verbose=False):
    """
    W[i]: work range of QC i
    """
    m, n, p = len(r), len(pD), np.add(pD, pL).tolist()
    
    # Revise so that minimum ready time is 0.
    r_min, r, d = prob.revise_to_zero(r, d)
    
    # Solve MIP.
    makespan, x, y, optimal = solve_MIP(r, d, p, H, ctime_limit, use_VC, verbose)
    
    if makespan is None:
        return [None] * 4
    
    W = prob.get_working_range(m, n, y)
    if verbose:
        print('    W =', W)
    
    #
    # Improve solution
    
    I = [i for i in range(m) if W[i] is not None]
    k0, k1 = 0, len(I) - 1
    i0, i1 = I[k0], I[k1]
    
    # Step 1: Make leftmost QC start from a hatch as left as possible.
    while W[i0][0] > 0:
        x, y = change_work_range('S1', i0, 0, -1, r, d, p, H, W, makespan, verbose)
    
    # Step 2: Make rightmost QC finish at a hatch as right as possible.
    while W[i1][1] < n - 1:
        x, y = change_work_range('S2', i1, 1, 1, r, d, p, H, W, makespan, verbose)
    
    while k0 < k1:
        # Step 3
        while W[i0][1] > W[i0][0]:
            xy1 = change_work_range('S3', i0, 1, -1, r, d, p, H, W, makespan, verbose)
            if xy1 is None:
                break
            x, y = xy1
        
        # Step 4
        while W[i1][0] < W[i1][1]:
            xy1 = change_work_range('S4', i1, 0, 1, r, d, p, H, W, makespan, verbose)
            if xy1 is None:
                break
            x, y = xy1
        
        # Move to next.
        k0, k1 = k0 + 1, k1 - 1
        if k1 < k0:
            break
        i0, i1 = I[k0], I[k1]
        
        # Step 5
        while W[i0][0] < W[i0][1]:
            xy1 = change_work_range('S5', i0, 0, 1, r, d, p, H, W, makespan, verbose)
            if xy1 is None:
                break
            x, y = xy1
        
        # Step 6
        while W[i1][1] > W[i0][0]:
            xy1 = change_work_range('S6', i1, 1, -1, r, d, p, H, W, makespan, verbose)
            if xy1 is None:
                break
            x, y = xy1
    
    #
    # Solution
    
    S, _pD, _pL = [[] for _ in range(m)], list(pD), list(pL)
    
    for i in reversed(range(m)):  # reverse because right QC handles first
        for j in range(n):
            if y[i][j] > EPSILON:
                # t: time, h: handling amount
                t, h = max(x[i][j], r[i]), y[i][j]
                
                if _pD[j] > EPSILON:
                    hD = min(h, _pD[j])
                    S[i].append([j, 'D', t, hD])
                    
                    t += hD
                    h -= hD
                    _pD[j] -= hD
                
                if _pL[j] > EPSILON and h > EPSILON:
                    hL = min(h, _pL[j])
                    S[i].append([j, 'L', t, hL])
                    
                    t += hL
                    _pL[j] -= hL
    
    T = [[] for _ in range(m)]
    
    for i in range(m):
        for j in range(n + 1):
            T[i].append([j, x[i][j]])
    
    # Recover solution from r_min-revision, if necessary.
    if r_min > 0:
        makespan, S, T = prob.revise_to_r_min(r_min, makespan, S, T)
    
    return makespan, S, optimal, T


def change_work_range(lbl, i, k, q, r, d, p, H, W, makespan, verbose):
    W[i][k] += q
    if verbose:
        print('%s: W = %s' % (lbl, W), end='')
    
    x, y = solve_LP(r, d, p, H, W, makespan)
    if x is not None:
        if verbose:
            print()
        return x, y
    
    W[i][k] -= q
    if verbose:
        print(' X')
    return None

    
def solve_MIP(r, d, p, H, ctime_limit, use_VC, verbose):
    """
    A[i], B[i]: whether QC i is not available from beginning and to end, respectively
    
    Decision variables
      x[i][j]: time from which QC i is located at hatch j 
      y[i][j]: processing amount of hatch j's job by QC i
      u[i][j]:
      v[i][j]:
    
    RETURN makespan, schedule S, optimality
      S[k]: [hatch, 'D' or 'L', start time, processing time]
    """
    
    m, n = len(r), len(p)
    d_max, avail = max(d), [d[i] - r[i] for i in range(m)]
    
    assert n >= 2 and all(ai > 0 for ai in avail)
    
    A = [r[i] > EPSILON for i in range(m)]
    B = [d[i] < d_max - EPSILON for i in range(m)]
    
    p1 = [[min(p[j], avail[i]) for j in range(n)] for i in range(m)]
    
    #
    # Model and variables
    
    mp = Problem('Hatch-wise')
    
    x = [[Var('x(%d,%d)' % (i, j), REAL, 0, d_max) for j in range(n + 1)]
         for i in range(m)]
    y = [[Var('y(%d,%d)' % (i, j), REAL, 0, p1[i][j]) for j in range(n)]
         for i in range(m)]
    u = [[Var('u(%d,%d)' % (i, j), BIN) for j in range(n)] if A[i] else None
         for i in range(m)]
    v = [[Var('v(%d,%d)' % (i, j), BIN) for j in range(n)] if B[i] else None
         for i in range(m)]
    
    #
    # Objective
    
    minimize(x[0][n])
    
    #
    # Constraints
    
    # TODO Lower bound, e.g., zmax >= 1137.25 - EPSILON. But I'm not sure it
    #   will be helpful, because LP objective value may not guide node-visiting
    #   order in that case (I am not even sure that guiding is done like what
    #   I guess). We need to check some materials.
    
    for i in range(m):
        for j in range(n):
            _C = y[i][j] <= x[i][j + 1] - x[i][j]
        
        if A[i]:
            for j in range(n):
                _C = y[i][j] <= p1[i][j] * (1 - u[i][j])
                _C = y[i][j] <= x[i][j + 1] - r[i] + r[i] * u[i][j]
        
        if B[i]:
            for j in range(n):
                _C = y[i][j] <= p1[i][j] * v[i][j]
                _C = y[i][j] <= d[i] - x[i][j] + (d_max - d[i]) * (1 - v[i][j])
    
    for j in range(n):
        _C = sum_(y[i][j] for i in range(m)) >= p[j]
    
    for i in range(m - 1):
        for j in range(n - 1):
            _C = x[i][j] >= x[i + 1][j + 2]
    
    # Cover range (TODO? Is it better to handle range constraints structurally?
    #   I first tried but it was not trivial. E.g., min. x[0][n] is not valid
    #   objective any more.)
    if H is not None:
        for i, (h0j, h1j) in enumerate(H):
            assert h0j <= h1j
            for j in range(0, h0j):
                    _C = y[i][j] == 0
            for j in range(h1j + 1, n):
                    _C = y[i][j] == 0
    
    #
    # Valid inequalities
    
    if use_VC:
        for i in range(m):
            if A[i]:
                for j in range(n - 1):
                    _C = u[i][j] >= u[i][j + 1]
                    
                for j in range(n):
                    _C = x[i][j + 1] <= r[i] + (d_max - r[i]) * (1 - u[i][j])
            
            if B[i]:
                for j in range(n - 1):
                    _C = v[i][j] >= v[i][j + 1]
                    
                for j in range(n):
                    _C = x[i][j] <= d[i] + (d_max - d[i]) * (1 - v[i][j])
    
        # Extras that are doubtable
        '''
        for i in range(m):
            if A[i]:
                for j in range(n):
                    _C = x[i][j + 1] >= r[i] - r[i] * u[i][j]
            
            if A[i] and B[i]:
                for j in range(n):
                    _C = u[i][j] <= v[i][j]
        '''
    
    if verbose and PRINT_MIP_FORMULATION:
        mp._print()
    
    #
    # Solve!
    
    mp.optimize(not verbose, ctime_limit)
    
    if not mp.is_solution:
        return [None] * 4
    
    #
    # Solution
    
    makespan = x[0][n].val
    x, y = [[xij.val for xij in xi] for xi in x], [[yij.val for yij in yi] for yi in y]
    
    return makespan, x, y, mp.is_solutionOptimal


def solve_LP(r, d, p, H, W, makespan):
    """
    """
    m, n = len(r), len(p)
    d_max, avail = max(d), [d[i] - r[i] for i in range(m)]
    
    p1 = [[min(p[j], avail[i]) for j in range(n)] for i in range(m)]
    
    #
    # Model and variables
    
    mp = Problem('Hatch-wise-LP')
    
    x = [[Var('x(%d,%d)' % (i, j), REAL, 0, d_max) for j in range(n + 1)]
         for i in range(m)]
    y = [[Var('y(%d,%d)' % (i, j), REAL, 0, p1[i][j]) for j in range(n)]
         for i in range(m)]
    
    #
    # Objective
    
    minimize(x[0][n])
    
    #
    # Constraints
    
    for i in range(m):
        if W[i] is not None:
            wi0, wi1 = W[i]
            
            for j in range(wi0):
                _C = x[i][j + 1] >= x[i][j]
                _C = y[i][j] == 0
            
            _C = y[i][wi0] <= x[i][wi0 + 1] - r[i]
            for j in range(wi0, wi1 + 1):
                _C = y[i][j] <= x[i][j + 1] - x[i][j]
            _C = y[i][wi1] <= d[i] - x[i][wi1]
            
            for j in range(wi1 + 1, n):
                _C = x[i][j + 1] >= x[i][j]
                _C = y[i][j] == 0
        
        else:
            for j in range(n):
                _C = x[i][j + 1] >= x[i][j]
                _C = y[i][j] == 0
    
    for i in range(m - 1):
        for j in range(n - 1):
            _C = x[i][j] >= x[i + 1][j + 2]
    
    for j in range(n):
        _C = sum_(y[i][j] for i in range(m)) >= p[j]
    
    if H is not None:
        for i, (h0j, h1j) in enumerate(H):
            for j in range(n):
                if j < h0j or j > h1j:
                    _C = y[i][j] == 0
    
    _C = x[0][n] <= makespan + EPSILON
    
    #
    # Solve!
    
    mp.optimize()
    
    if not mp.is_solution:
        return None, None
    
    #
    # Solution
    
    x, y = [[xij.val for xij in xi] for xi in x], [[yij.val for yij in yi] for yi in y]
    assert np.isclose(makespan, x[0][n])
    
    return x, y


def test():
    pb = prob.ex_h4()
    #pb = [25, 29, 33], [140, 140, 140], [0, 30, 7, 1, 30, 11, 33, 3, 28, 21], [30, 28, 5, 5, 11, 0, 18, 31, 19, 24]
    
    makespan, S, optimal, T = solve(*pb, None, 1000, use_VC=True, verbose=True)
    
    print('#' * 80, '\n', makespan, optimal)
    for i, Si in enumerate(S):
        print(i, Si)
    print('#' * 80)
    for i, Ti in enumerate(T):
        print(i, Ti)


if __name__ == '__main__':
    test()
