from functools import reduce
from importlib import import_module
from time import time

import numpy as np

from ctbike.qcs import prob


SOLVER_MODULE = [('hatch-wise', 'hw'), ('discharge-first', 'df')]


def solve(pattern, r, d, pD, pL, H=None, QCIs=None, ctime_limit=10, verbose=False):
    """
    H: workable hatch range of QCs (e.g., [(0, 3), (2, 3)] means QC0 can handle
      hatches from 0 to 3 (inclusive)) (H can be None.)
    QCIs: list of QC indices to be considered (e.g., [(1,2), (3,4)] means 
      first to consider QC1 and QC2 and next to consider QC3 and QC4
    ctime_limit: MIP-solving time in sec
    
    RETURN list of [used QC indices, makespan, schedule S, optimality, trajectory T]
      S[k]: [hatch, 'D' or 'L', start time, processing time]
    
    TODO Duality gap?? (MICPL seems not to provide gap. --)
    """
    assert all(ri < di for ri, di in zip(r, d))
    
    m, n = len(r), len(pD)
    
    # Prepare H.
    print(H)
    if H is None:
        H = [(0, n - 1)] * m
    else:
        assert len(H) == m
        H = [tuple(Hi) for Hi in H]
    
    if verbose:
        print('#' * 80, '\n### Solve *' + pattern + '* with d_max', max(d))
        print(' --  r: %s\n --  d: %s\n -- pD: %s\n -- pL: %s\n --  H: %s' %
              (r, d, pD, pL, H))
    
    # Get solution module.
    sm = reduce(lambda ptn, kv: ptn.replace(*kv), SOLVER_MODULE, pattern.lower())
    solve_fn = import_module('ctbike.qcs.' + sm).solve
    
    # Populate all possible QC indices to be considered, if not specified.
    if QCIs is None:
        QCIs = [[*range(i0, i0 + k + 1)] for k in range(m) for i0 in range(m - k)]
    
    # Rs: list of results
    # rdHI_R: dict of {[ready times, deadlines, cover ranges] --> results}
    Rs, rdHI_R = [], {}
    
    for QCI in QCIs:
        # Ready times and deadlines of QCI
        rI, dI, HI = [tuple(_x[i] for i in QCI) for _x in (r, d, H)]
        
        if verbose:
            print('#' * 80)
            print("Consider: %s\n r', d', H' = %s, %s, %s" % (QCI, rI, dI, [*HI]))
        
        # Solve or reuse previous.
        R = rdHI_R.get((rI, dI, HI))
        if R is None:
            makespan, S, optimal, T = solve_fn(rI, dI, pD, pL, HI, ctime_limit,
                                               verbose= verbose)
            rdHI_R[(rI, dI, HI)] = [makespan, S, optimal, T]
            
            if makespan is not None:
                if verbose:
                    print('makespan = %.1f' % makespan)
                
                # Check validity (makespan and processing time).
                prob.check_validity(rI, dI, pD, pL, makespan, S)
        
        else:
            makespan, S, optimal, T = R 
            if verbose:
                print(' -- Reusing previous solution with makespan:', makespan)
        
        if makespan is not None:
            Rs.append([QCI, makespan, S, optimal, T])
    
    if verbose:
        print('#' * 80)
    
    return Rs


def test():
    R = solve('Hatch-wise3', *prob.ex1(), None, 10, verbose=True)
    for result in R:
        print(result)


def test_compare():
    from ctbike.qcs import df3 as algo1, df3 as algo2
    settings = prob.EX_GEN_AB_4_15
    
    for i in range(10000):
        if (i + 1) % 100 == 0:
            print('[%d]' % (i + 1))
        
        pb = prob.generate(*settings)
        
        _t0 = time()
        ms1 = algo1.solve(*pb, None, 10000, use_VC=False)[0]
        _t1 = time()
        ms2 = algo2.solve(*pb, None, 10000, use_VC=True)[0]
        _t2 = time()
        
        print('%.1f %.1f (%d%%)' % (_t1 - _t0, _t2 - _t1, (_t2 - _t1) / (_t1 - _t0) * 100))
        
        ok = ((ms1 is None and ms2 is None) or
              (ms1 is not None and ms2 is not None and np.isclose(ms1, ms2)))
        if not ok:
            print(i, pb, ms1, ms2)
            assert False


if __name__ == '__main__':
    #test()
    test_compare()
