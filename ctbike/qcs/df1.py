"""
Discharge-first
"""

import numpy as np

from mipcl_py.mipshell.mipshell import Problem, minimize, Var, sum_, BIN

from ctbike.qcs import prob
EPSILON = prob.EPSILON 


PRINT_MIP_FORMULATION = 0


def solve(r, d, pD, pL, H=None, ctime_limit=10, verbose=False):
    """
    xD[i][j], xL[i][j]: processing amount of hatch j's D/L job by crane i
    yD[i][j], yL[i][j]: 1, if xX[i][j] > 0; 0, otherwise
    zD[i][j], zL[i][j]: completion time of hatch j's D/L job by crane i
    
    RETURN makespan, schedule S, optimality
      S[k]: [hatch, 'D' or 'L', start time, processing time]
    """
    n, m = len(pD), len(r)
    r_min, d_max = min(r), max(d)
    
    assert np.less_equal(r, d).all()
    assert H is None or all([*Hi] == [0, n - 1] for Hi in H), H
    
    #
    # Model and variables
    
    mp = Problem('discharge-first')
    
    # NOTE Default LB of variables is 0.
    xD = [[Var('xD(%d,%d)' % (i, j)) for j in range(n)] for i in range(m)]
    xL = [[Var('xL(%d,%d)' % (i, j)) for j in range(n)] for i in range(m)]
    yD = [[Var('yD(%d,%d)' % (i, j), BIN) for j in range(n)] for i in range(m)]
    yL = [[Var('yL(%d,%d)' % (i, j), BIN) for j in range(n)] for i in range(m)]
    zD = [[Var('zD(%d,%d)' % (i, j)) for j in range(n)] for i in range(m)]
    zL = [[Var('zL(%d,%d)' % (i, j)) for j in range(n)] for i in range(m)]
    zmax = Var('zmax')
    
    #
    # Objective
    
    minimize(zmax)
    
    #
    # Constraints
    
    # zmax
    for i in range(m):
        _C = zmax >= zD[i][-1]
        _C = zmax >= zL[i][0]
    
    # Define xD and xL (handling all jobs).
    for j in range(n):
        _C = sum_(xD[i][j] for i in range(m)) >= pD[j]
        _C = sum_(xL[i][j] for i in range(m)) >= pL[j]
    
    # Define zD and zL (completion time considering a QC's job handling).
    for i in range(m):
        for j in range(n - 1):
            _C = zD[i][j + 1] >= zD[i][j] + xD[i][j + 1]
            _C = zL[i][j] >= zL[i][j + 1] + xL[i][j]
    
    # Avoiding interference
    for i in range(m - 1):
        for j in range(n - 1):
            _C = zD[i][j] >= zD[i + 1][j + 1] + xD[i][j]
            _C = zL[i + 1][j + 1] >= zL[i][j] + xL[i + 1][j + 1]
        
    # Define yD and yL.
    for i in range(m):
        for j in range(n):
            _C = xD[i][j] <= pD[j] * yD[i][j]
            _C = xL[i][j] <= pL[j] * yL[i][j]
    
    # Connect discharge and loading.
    M = sum(pD)  # TODO
    for i in range(m):
        for j in range(n):
            _C = zL[i][j] >= zD[i][j] + xL[i][j] - M * (1 - yD[i][j])
            _C = zL[i][j] >= zD[i][j] + xL[i][j] - M * (1 - yL[i][j])
    
    # Constrain completion time by ready time and deadline.
    for i in range(m):
        if r[i] <= r_min + EPSILON:
            for j in range(n):
                _C = zD[i][j] >= r_min + xD[i][j]
                _C = zL[i][j] >= r_min + xL[i][j]
        else:
            for j in range(n):
                _C = zD[i][j] >= r[i] + xD[i][j] - (r[i] + pD[j]) * (1 - yD[i][j])
                _C = zL[i][j] >= r[i] + xL[i][j] - (r[i] + pL[j]) * (1 - yL[i][j])
        
        if d[i] >= d_max - EPSILON:
            for j in range(n):
                _C = zD[i][j] <= d_max
                _C = zL[i][j] <= d_max
        else:
            for j in range(n):
                _C = zD[i][j] <= d[i] + (d_max - d[i]) * (1 - yD[i][j])
                _C = zL[i][j] <= d[i] + (d_max - d[i]) * (1 - yL[i][j])
    
    if verbose and PRINT_MIP_FORMULATION:
        mp._print()
    
    #
    # Solve!
    
    mp.optimize(not verbose, ctime_limit)
    
    if not mp.is_solution:
        return [None] * 4
    
    #
    # Solution
    
    makespan, S = 0, [[] for _ in r]
    pD1, pL1 = list(pD), list(pL)
    
    for i in reversed(range(m)):  # reverse because right QC handles first
        for j in range(n):
            if xD[i][j].val > EPSILON:
                xDij, zDij = xD[i][j].val, zD[i][j].val
                
                if pD1[j] > EPSILON:
                    xd = min(xDij, pD1[j])
                    S[i].append([j, 'D', zDij - xDij, xd])
                    
                    pD1[j] -= xd
                
    for i in range(m):
        for j in reversed(range(n)):
            if xL[i][j].val > EPSILON:
                xLij, zLij = xL[i][j].val, zL[i][j].val
                
                if pL1[j] > EPSILON:
                    xl = min(xLij, pL1[j])
                    S[i].append([j, 'L', zLij - xLij, xl])
                    
                    pL1[j] -= xl
                
                # TODO Correct evaluation considering deadline
                if zLij > makespan:
                    makespan = zLij
    
    return makespan, S, mp.is_solutionOptimal, None


def test():
    pb = prob.ex_h1()
    
    makespan, S, optimal, _ = solve(*pb, None, 1000, verbose=True)
    
    print('#' * 80, '\n', makespan, optimal)
    for Si in S:
        print(Si)


if __name__ == '__main__':
    test()
