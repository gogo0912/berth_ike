"""
Hatch-wise with yet another approach
"""

import numpy as np

from mipcl_py.mipshell.mipshell import Problem, minimize, Var, sum_, BIN

from ctbike.qcs import prob
EPSILON = prob.EPSILON 


PRINT_MIP_FORMULATION = 0


def solve(r, d, pD, pL, H=None, ctime_limit=10, verbose=False):
    """
    A[i], B[i]: whether QC i is available from beginning and to end, respectively
    
    Decision variables
      x[i][j]: time from which QC i is located at hatch j 
      y[i][j]: processing amount of hatch j's job by QC i
      u[i][j]: whether hatch j is where QC i stands at ready time
      v[i][j]: whether hatch j is where QC i stands at deadline
    
    RETURN makespan, schedule S, optimality
      S[k]: [hatch, 'D' or 'L', start time, processing time]
    """
    n, m, p = len(pD), len(r), np.add(pD, pL).tolist()
    r_min, d_max = min(r), max(d)
    
    assert n > 2 and np.less(r, d).all()
    assert H is None or all([*Hi] == [0, n - 1] for Hi in H), H
    
    A = [r[i] <= r_min + EPSILON for i in range(m)]
    B = [d[i] >= d_max - EPSILON for i in range(m)]
    
    #
    # Model and variables
    
    mp = Problem('Hatch-wise')
    
    # NOTE Default LB of variables is 0.
    x = [[Var('x(%d,%d)' % (i, j)) for j in range(n + 1)] for i in range(m)]
    y = [[Var('y(%d,%d)' % (i, j)) for j in range(n)] for i in range(m)]
    u = [None if A[i] else [Var('u(%d,%d)' % (i, j), BIN) for j in range(n)]
         for i in range(m)]
    v = [None if B[i] else [Var('v(%d,%d)' % (i, j), BIN) for j in range(n)]
         for i in range(m)]
    xmax = Var('xmax')
    
    #
    # Objective
    
    minimize(xmax)
    
    #p_max = max(p)
    #minimize(n * m * (n * m + 1) / 2 * p_max * xmax + sum_(((n - 1) * i * (i + 1) // 2 + i + j * (i + 1)) * y[i][j] for i in range(m) for j in range(n)))
    
    for i in range(m):
        _C = xmax >= x[i][n]
    
    #
    # Constraints
    
    # TODO Lower bound, e.g., zmax >= 1137.25 - 0.0000000001. But I'm not sure
    #   it will be helpful, because objective value may not guide node-visiting
    #   order in that case (I am not even sure that guiding is done like what
    #   I guess). We need to check some materials.
    
    # Define x.
    for i in range(m):
        for j in range(n):
            _C = x[i][j] <= x[i][j + 1]
    
    for i in range(m - 1):
        for j in range(n - 1):
            _C = x[i][j] >= x[i + 1][j + 2]
    
    for i in range(m):
        a_i = d[i] - r[i]  # available time
        
        for j in range(n):
            _C = y[i][j] <= x[i][j + 1] - x[i][j]
            _C = y[i][j] <= min(p[j], a_i)
            
        if A[i]:
            _C = x[i][0] >= r_min
        
        else:
            _C = sum_(u[i][j] for j in range(n)) == 1
            
            for j in range(n):
                _C = y[i][j] <= x[i][j + 1] - r[i] + (r[i] - r_min) * (1 - u[i][j])
                
                for k in range(j):
                    _C = y[i][k] <= min(p[k], a_i) * (1 - u[i][j])
        
        if B[i]:
            _C = x[i][n] <= d_max
        
        else:
            _C = sum_(v[i][j] for j in range(n)) == 1
            
            for j in range(n):
                _C = y[i][j] <= d[i] - x[i][j] + (d_max - d[i]) * (1 - v[i][j])
                
                for k in range(j + 1, n):
                    _C = y[i][k] <= min(p[k], a_i) * (1 - v[i][j])
    
    # Handling all jobs.
    for j in range(n):
        _C = sum_(y[i][j] for i in range(m)) >= p[j]
    
    # Valid inequalities
    
    for i in range(m):
        if not A[i]:
            for j in range(n):
                _C = x[i][j] <= r[i] + (d_max - r[i]) * (1 - u[i][j])
                ''' similar but not much effective (sometimes slower)
                for k in range(j + 1):
                    _C = x[i][k] <= r[i] + (d_max - r[i]) * (1 - u[i][j])
                '''
    
    for i in range(m):
        if not A[i] and not B[i]:
            for j in range(n):
                for k in range(j):
                    _C = v[i][k] <= 1 - u[i][j]
                for k in range(j + 1, n):
                    _C = u[i][k] <= 1 - v[i][j]
    
    
    ''' invalid
    for i in range(m):
        if not B[i]:
            for j in range(n):
                _C = x[i][j + 1] >= d[i] + (r_min - d[i]) * (1 - v[i][j])
    '''
    
    if verbose and PRINT_MIP_FORMULATION:
        mp._print()
    
    #
    # Solve!
    
    mp.optimize(not verbose, ctime_limit)
    
    if not mp.is_solution:
        return [None] * 4
    
    #
    # Solution
    
    makespan, S = 0, [[] for _ in r]
    pD1, pL1 = list(pD), list(pL)
    
    for i in reversed(range(m)):  # reverse because right QC handles first
        for j in range(n):
            if y[i][j].val > EPSILON:
                # t: time, h: handling amount
                t, h = max(x[i][j].val, r[i]), y[i][j].val
                
                if pD1[j] > EPSILON:
                    hD = min(h, pD1[j])
                    S[i].append([j, 'D', t, hD])
                    
                    t += hD
                    h -= hD
                    pD1[j] -= hD
                
                if pL1[j] > EPSILON and h > EPSILON:
                    hL = min(h, pL1[j])
                    S[i].append([j, 'L', t, hL])
                    
                    t += hL
                    pL1[j] -= hL
                
                if t > makespan:
                    makespan = t
    
    return makespan, S, mp.is_solutionOptimal, None


def test():
    pb = prob.ex_h4()
    #pb = [0, 0], [140, 126], [22, 19, 24, 7, 15, 0], [17, 16, 7, 21, 0, 20]
    
    makespan, S, optimal, _ = solve(*pb, None, 1000, True)
    
    print('#' * 80, '\n', makespan, optimal)
    for Si in S:
        print(Si)


if __name__ == '__main__':
    test()
