"""
Discharge-first
"""

import numpy as np

from mipcl_py.mipshell.mipshell import Problem, minimize, Var, sum_, BIN, REAL

from ctbike.qcs import prob
EPSILON = prob.EPSILON 


PRINT_MIP_FORMULATION = 0


def solve(r, d, pD, pL, H=None, ctime_limit=10, use_VC=True, verbose=False):
    """
    A[i], B[i]: whether QC i is not available from beginning and to end, respectively
    
    Decision variables (T as D or L)
      xD
      ...
    
    RETURN makespan, schedule S, optimality
      S[k]: [hatch, 'D' or 'L', start time, processing time]
    """
    # Revise so that minimum ready time is 0.
    r_min, r, d = prob.revise_to_zero(r, d)
    
    m, n = len(r), len(pD)
    d_max, avail = max(d), [d[i] - r[i] for i in range(m)]
    
    assert n >= 2 and np.less(r, d).all()
    
    A = [r[i] > EPSILON for i in range(m)]
    B = [d[i] < d_max - EPSILON for i in range(m)]
    
    pD1 = [[min(pD[j], avail[i]) for j in range(n)] for i in range(m)]
    pL1 = [[min(pL[j], avail[i]) for j in range(n)] for i in range(m)]
    
    #
    # Model and variables
    
    mp = Problem('Discharge-first')
    
    xD = [[Var('xD(%d,%d)' % (i, j), REAL, 0, d_max) for j in range(n + 1)]
          for i in range(m)]
    xL = [[Var('xL(%d,%d)' % (i, j), REAL, 0, d_max) for j in range(n + 1)]
          for i in range(m)]
    yD = [[Var('yD(%d,%d)' % (i, j), REAL, 0, pD1[i][j]) for j in range(n)]
          for i in range(m)]
    yL = [[Var('yL(%d,%d)' % (i, j), REAL, 0, pL1[i][j]) for j in range(n)]
          for i in range(m)]
    uD = [[Var('uD(%d,%d)' % (i, j), BIN) for j in range(n)] if A[i] else None
          for i in range(m)]
    uL = [[Var('uL(%d,%d)' % (i, j), BIN) for j in range(n)] if A[i] else None
          for i in range(m)]
    vD = [[Var('vD(%d,%d)' % (i, j), BIN) for j in range(n)] if B[i] else None
          for i in range(m)]
    vL = [[Var('vL(%d,%d)' % (i, j), BIN) for j in range(n)] if B[i] else None
          for i in range(m)]
    w = [[Var('w(%d,%d)' % (i, j), BIN) for j in range(n)] for i in range(m)]
    
    #
    # Objective
    
    minimize(xL[m - 1][0])
    
    #
    # Constraints
    
    for i in range(m):
        for j in range(n):
            _C = yD[i][j] <= xD[i][j + 1] - xD[i][j]
            _C = yL[i][j] <= xL[i][j] - xL[i][j + 1]
        
        if A[i]:
            for j in range(n):
                _C = yD[i][j] <= pD1[i][j] * (1 - uD[i][j])
                _C = yD[i][j] <= xD[i][j + 1] - r[i] + r[i] * uD[i][j]
                
                _C = yL[i][j] <= pL1[i][j] * (1 - uL[i][j])
                _C = yL[i][j] <= xL[i][j] - r[i] + r[i] * uL[i][j]
        
        if B[i]:
            for j in range(n):
                _C = yD[i][j] <= pD1[i][j] * vD[i][j]
                _C = yD[i][j] <= d[i] - xD[i][j] + (d_max - d[i]) * (1 - vD[i][j])
                
                _C = yL[i][j] <= pL1[i][j] * vL[i][j]
                _C = yL[i][j] <= d[i] - xL[i][j + 1] + (d_max - d[i]) * (1 - vL[i][j])
    
    for i in range(m - 1):
        for j in range(n - 1):
            _C = xD[i][j] >= xD[i + 1][j + 2]
            _C = xL[i][j] <= xL[i + 1][j + 2]
    
    for j in range(n):
        _C = sum_(yD[i][j] for i in range(m)) >= pD[j]
        _C = sum_(yL[i][j] for i in range(m)) >= pL[j]
    
    # Connect discharge and loading.
    for i in range(m):
        for j in range(n):
            _C = yD[i][j] + yL[i][j] <= xL[i][j] - xD[i][j] + d_max * (1 - w[i][j])
            _C = yD[i][j] + yL[i][j] <= min(pD1[i][j] + pL1[i][j], avail[i])
            
            _C = yD[i][j] <= pD1[i][j] * w[i][j]
            _C = yL[i][j] <= pL1[i][j] * w[i][j]
        
        if A[i]:
            for j in range(n):
                _C = yD[i][j] + yL[i][j] <= xL[i][j] - r[i] + r[i] * uL[i][j]
                _C = yD[i][j] <= pD1[i][j] * (1 - uL[i][j])  # strange but indispensable
        
        if B[i]:
            for j in range(n):
                _C = yD[i][j] + yL[i][j] <= d[i] - xD[i][j] + (d_max - d[i]) * (1 - vD[i][j])
                _C = yL[i][j] <= pL1[i][j] * vD[i][j]  # strange but indispensable
    
    # Cover range
    if H is not None:
        for i, (h0j, h1j) in enumerate(H):
            for j in range(0, h0j):
                    _C = yD[i][j] == 0
                    _C = yL[i][j] == 0
            for j in range(h1j + 1, n):
                    _C = yD[i][j] == 0
                    _C = yL[i][j] == 0
    
    #
    # Valid inequalities
    
    if use_VC:
        for i in range(m):
            if A[i]:
                for j in range(n - 1):
                    _C = uD[i][j] >= uD[i][j + 1]
                    _C = uL[i][j + 1] >= uL[i][j]
                
                for j in range(n):
                    _C = xD[i][j + 1] <= r[i] + (d_max - r[i]) * (1 - uD[i][j])
                    _C = xL[i][j] <= r[i] + (d_max - r[i]) * (1 - uL[i][j])
            
            if B[i]:
                for j in range(n - 1):
                    _C = vD[i][j] >= vD[i][j + 1]
                    _C = vL[i][j + 1] >= vL[i][j]
                    
                for j in range(n):
                    _C = xD[i][j] <= d[i] + (d_max - d[i]) * (1 - vD[i][j])
                    _C = xL[i][j + 1] <= d[i] + (d_max - d[i]) * (1 - vL[i][j])
            
        
        for i in range(m):
            for j in range(n - 1):
                _C = w[i][j] >= w[i][j + 1]
        
        for i in range(m - 1):
            for j in range(n - 2):
                _C = w[i + 1][j + 2] >= w[i][j]
    
    # Invalid inequalities
    '''
    for i in range(m):
        if A[i]:
            for j in range(n - 1):
                # xD[k..n] and xL[l..n] should float freely.
                _C = uD[i][n - 1] >= uL[i][n - 1]
        if B[i]:
            for j in range(n - 1):
                # With same reason above.
                _C = vL[i][n - 1] >= vD[i][n - 1]
    '''
    
    if verbose and PRINT_MIP_FORMULATION:
        mp._print()
    
    #
    # Solve!
    
    mp.optimize(not verbose, ctime_limit)
    
    if not mp.is_solution:
        return [None] * 4
    
    #
    # Solution
    
    makespan = xL[m - 1][0].val
    
    S, _pD, _pL = [[] for _ in range(m)], list(pD), list(pL)
    
    for i in reversed(range(m)):  # reverse because right QC handles first
        for j in range(n):
            if yD[i][j].val > EPSILON and _pD[j] > EPSILON:
                xd = min(yD[i][j].val, _pD[j])
                S[i].append([j, 'D', max(xD[i][j].val, r[i]), xd])
                _pD[j] -= xd
    
    for i in range(m):
        for j in reversed(range(n)):
            if yL[i][j].val > EPSILON and _pL[j] > EPSILON:
                xl = min(yL[i][j].val, _pL[j])
                s = max(max(xD[i][j].val, r[i]) + yD[i][j].val, xL[i][j + 1].val, r[i])
                S[i].append([j, 'L', s, xl])
                _pL[j] -= xl
    
    T = [[] for _ in range(m)]
    
    for i in range(m):
        for jD in range(n + 1):
            if jD == n or w[i][jD].val < EPSILON:
                # NOTE This works because of constraint, w[i][j] >= w[i][j + 1]. 
                break
            T[i].append([jD, xD[i][jD].val])
        if jD > 0:
            T[i].append([jD - 1, xL[i][jD - 1].val])  # This is for correct drawing.
            for jL in reversed(range(jD)):
                T[i].append([jL - 1, xL[i][jL].val])
    
    # Recover solution from r_min-revision, if necessary.
    if r_min > 0:
        makespan, S, T = prob.revise_to_r_min(r_min, makespan, S, T)
    
    return makespan, S, mp.is_solutionOptimal, T


def test():
    #pb = prob.ex_h1()
    pb = [0, 0, 19], [85, 140, 137], [35, 29, 28, 16, 4, 35, 7, 7, 31, 2], [6, 22, 36, 0, 27, 2, 27, 17, 0, 17]
    use_VC = True
    
    makespan, S, optimal, T = solve(*pb, None, 1000, use_VC, verbose=True)
    
    print('#' * 80, '\n', makespan, optimal)
    for Si in S:
        print(Si)
    print('#' * 80)
    for i, Ti in enumerate(T):
        print(i, Ti)


if __name__ == '__main__':
    test()
