"""
Hatch-wise
"""

import numpy as np

from mipcl_py.mipshell.mipshell import Problem, minimize, Var, sum_, BIN

from ctbike.qcs import prob
EPSILON = prob.EPSILON 


PRINT_MIP_FORMULATION = 0


def solve(r, d, pD, pL, H=None, ctime_limit=10, verbose=False):
    """
    A[i], B[i]: whether QC i is available from beginning and to end, respectively
    
    Decision variables
      x[i][j]: processing amount of hatch j's job by QC i
      y[i][j]: 1, if x[i][j] > 0; 0, otherwise
      z[i][j]: completion time of hatch j's job by QC i
      s[i][j]: start time of hatch j's job by QC i
    
    RETURN makespan, schedule S, optimality
      S[k]: [hatch, 'D' or 'L', start time, processing time]
    """
    n, m, p = len(pD), len(r), np.add(pD, pL).tolist()
    r_min, d_max = min(r), max(d)
    
    assert np.less(r, d).all()
    assert H is None or all([*Hi] == [0, n - 1] for Hi in H), H
    
    A = [r[i] <= r_min + EPSILON for i in range(m)]
    B = [d[i] >= d_max - EPSILON for i in range(m)]
    
    #
    # Model and variables
    
    mp = Problem('Hatch-wise')
    
    # NOTE Default LB of variables is 0.
    x = [[Var('x(%d,%d)' % (i, j)) for j in range(n)] for i in range(m)]
    y = [None if A[i] and B[i] else [Var('y(%d,%d)' % (i, j), BIN) for j in range(n)]
         for i in range(m)]
    z = [[Var('z(%d,%d)' % (i, j)) for j in range(n)] for i in range(m)]
    zmax = Var('zmax')
    
    s = [[Var('s(%d,%d)' % (i, j)) for j in range(n)] for i in range(m)]
    
    #
    # Objective
    
    minimize(zmax)
    
    for i in range(m):
        _C = zmax >= z[i][-1]
    
    #
    # Constraints
    
    # TODO Lower bound, e.g., zmax >= 1137.25 - 0.0000000001. But I'm not sure
    #   it will be helpful, because objective value cannot guide node-visiting
    #   order in that case. We need to check some materials.
    
    # Define x (handling all jobs).
    for j in range(n):
        _C = sum_(x[i][j] for i in range(m)) >= p[j]
    
    # Define z (completion time considering a QC's job handling).
    for i in range(m):
        #for j in range(n - 1):
        #    _C = z[i][j + 1] >= z[i][j] + x[i][j + 1]
        
        for j in range(n):
            _C = z[i][j] == s[i][j] + x[i][j]
        
        for j in range(n - 1):
            _C = s[i][j + 1] >= z[i][j]
    
    # Avoiding interference
    for i in range(m - 1):
        for j in range(n - 1):
            #_C = z[i][j] >= z[i + 1][j + 1] + x[i][j]
            _C = s[i][j] >= z[i + 1][j + 1]
    
    # Define y.
    for i in range(m):
        if A[i] and B[i]:
            for j in range(n):
                _C = x[i][j] <= p[j]
        else:
            for j in range(n):
                _C = x[i][j] <= p[j] * y[i][j]
    
    # Constrain completion time by ready time and deadline.
    for i in range(m):
        if A[i]:
            # NOTE Sufficient: s[i][0] >= r_min
            for j in range(n):
                _C = s[i][j] >= r_min
        else:
            for j in range(n):
                #_C = z[i][j] >= r[i] + x[i][j] - (r[i] + p[j]) * (1 - y[i][j])
                _C = s[i][j] >= r[i] * y[i][j]
        
        if B[i]:
            # NOTE Sufficient: z[i][-1] <= d_max
            for j in range(n):
                _C = z[i][j] <= d_max
        else:
            for j in range(n):
                _C = z[i][j] <= -(d_max - d[i]) * y[i][j] + d_max
    
    if verbose and PRINT_MIP_FORMULATION:
        mp._print()
    
    #
    # Solve!
    
    mp.optimize(not verbose, ctime_limit)
    
    if not mp.is_solution:
        return [None] * 4
    
    #
    # Solution
    
    makespan, S = 0, [[] for _ in r]
    pD1, pL1 = list(pD), list(pL)
    
    for i in reversed(range(m)):  # reverse because right QC handles first
        for j in range(n):
            if x[i][j].val > EPSILON:
                xij, zij = x[i][j].val, z[i][j].val
                
                if pD1[j] > EPSILON:
                    xd = min(xij, pD1[j])
                    S[i].append([j, 'D', zij - xij, xd])
                    
                    xij -= xd
                    pD1[j] -= xd
                
                if pL1[j] > EPSILON and xij > EPSILON:
                    xl = min(xij, pL1[j])
                    S[i].append([j, 'L', zij - xij, xl])
                    
                    pL1[j] -= xl
                
                if zij > makespan:
                    makespan = zij
    
    '''
    if verbose:
        for i in range(m):
            print('QC%d: %.0f -- %.0f' % (i, u[i].val, v[i].val))
    '''
    
    return makespan, S, mp.is_solutionOptimal, None


def test():
    pb = prob.ex_h3()
    
    makespan, S, optimal, _ = solve(*pb, None, 1000, verbose=True)
    
    print('#' * 80, '\n', makespan, optimal)
    for Si in S:
        print(Si)


if __name__ == '__main__':
    test()
