"""
Hatch-wise with yet another approach
"""

import numpy as np

from mipcl_py.mipshell.mipshell import Problem, minimize, Var, sum_, REAL, BIN

from ctbike.qcs import prob
EPSILON = prob.EPSILON 


PRINT_MIP_FORMULATION = 0


def solve(r, d, pD, pL, H=None, ctime_limit=10, use_VC=True, verbose=False):
    """
    A[i], B[i]: whether QC i is not available from beginning and to end, respectively
    
    Decision variables
      x[i][j]: time from which QC i is located at hatch j 
      y[i][j]: processing amount of hatch j's job by QC i
      u[i][j]:
      v[i][j]:
    
    RETURN makespan, schedule S, optimality
      S[k]: [hatch, 'D' or 'L', start time, processing time]
    """
    # Revise so that minimum ready time is 0.
    r_min, r, d = prob.revise_to_zero(r, d)
    
    m, n, p = len(r), len(pD), np.add(pD, pL).tolist()
    d_max, avail = max(d), [d[i] - r[i] for i in range(m)]
    
    assert n >= 2 and all(ai > 0 for ai in avail)
    
    A = [r[i] > EPSILON for i in range(m)]
    B = [d[i] < d_max - EPSILON for i in range(m)]
    
    p1 = [[min(p[j], avail[i]) for j in range(n)] for i in range(m)]
    
    #
    # Model and variables
    
    mp = Problem('Hatch-wise')
    
    x = [[Var('x(%d,%d)' % (i, j), REAL, 0, d_max) for j in range(n + 1)]
         for i in range(m)]
    y = [[Var('y(%d,%d)' % (i, j), REAL, 0, p1[i][j]) for j in range(n)]
         for i in range(m)]
    u = [[Var('u(%d,%d)' % (i, j), BIN) for j in range(n)] if A[i] else None
         for i in range(m)]
    v = [[Var('v(%d,%d)' % (i, j), BIN) for j in range(n)] if B[i] else None
         for i in range(m)]
    
    #
    # Objective
    
    minimize(x[0][n])
    
    #
    # Constraints
    
    # TODO Lower bound, e.g., zmax >= 1137.25 - EPSILON. But I'm not sure it
    #   will be helpful, because LP objective value may not guide node-visiting
    #   order in that case (I am not even sure that guiding is done like what
    #   I guess). We need to check some materials.
    
    for i in range(m):
        for j in range(n):
            _C = y[i][j] <= x[i][j + 1] - x[i][j]
        
        if A[i]:
            for j in range(n):
                _C = y[i][j] <= p1[i][j] * (1 - u[i][j])
                _C = y[i][j] <= x[i][j + 1] - r[i] + r[i] * u[i][j]
        
        if B[i]:
            for j in range(n):
                _C = y[i][j] <= p1[i][j] * v[i][j]
                _C = y[i][j] <= d[i] - x[i][j] + (d_max - d[i]) * (1 - v[i][j])
    
    for i in range(m - 1):
        for j in range(n - 1):
            _C = x[i][j] >= x[i + 1][j + 2]
    
    for j in range(n):
        _C = sum_(y[i][j] for i in range(m)) >= p[j]
    
    # Cover range (TODO? Is it better to handle range constraints structurally?
    #   I first tried but it was not trivial. E.g., min. x[0][n] is not valid
    #   objective any more.)
    if H is not None:
        for i, (h0j, h1j) in enumerate(H):
            assert h0j <= h1j
            for j in range(0, h0j):
                    _C = y[i][j] == 0
            for j in range(h1j + 1, n):
                    _C = y[i][j] == 0
    
    #
    # Valid inequalities
    
    if use_VC:
        for i in range(m):
            if A[i]:
                for j in range(n - 1):
                    _C = u[i][j] >= u[i][j + 1]
                    
                for j in range(n):
                    _C = x[i][j + 1] <= r[i] + (d_max - r[i]) * (1 - u[i][j])
            
            if B[i]:
                for j in range(n - 1):
                    _C = v[i][j] >= v[i][j + 1]
                    
                for j in range(n):
                    _C = x[i][j] <= d[i] + (d_max - d[i]) * (1 - v[i][j])
    
        # Extras that are doubtable
        '''
        for i in range(m):
            if A[i]:
                for j in range(n):
                    _C = x[i][j + 1] >= r[i] - r[i] * u[i][j]
            
            if A[i] and B[i]:
                for j in range(n):
                    _C = u[i][j] <= v[i][j]
        '''
    
    if verbose and PRINT_MIP_FORMULATION:
        mp._print()
    
    #
    # Solve!
    
    mp.optimize(not verbose, ctime_limit)
    
    if not mp.is_solution:
        return [None] * 4
    
    #
    # Solution
    
    makespan = x[0][n].val
    
    S, _pD, _pL = [[] for _ in range(m)], list(pD), list(pL)
    
    for i in reversed(range(m)):  # reverse because right QC handles first
        for j in range(n):
            if y[i][j].val > EPSILON:
                # t: time, h: handling amount
                t, h = max(x[i][j].val, r[i]), y[i][j].val
                
                if _pD[j] > EPSILON:
                    hD = min(h, _pD[j])
                    S[i].append([j, 'D', t, hD])
                    
                    t += hD
                    h -= hD
                    _pD[j] -= hD
                
                if _pL[j] > EPSILON and h > EPSILON:
                    hL = min(h, _pL[j])
                    S[i].append([j, 'L', t, hL])
                    
                    t += hL
                    _pL[j] -= hL
    
    T = [[] for _ in range(m)]
    
    for i in range(m):
        for j in range(n + 1):
            T[i].append([j, x[i][j].val])
    
    # Recover solution from r_min-revision, if necessary.
    if r_min > 0:
        makespan, S, T = prob.revise_to_r_min(r_min, makespan, S, T)
    
    return makespan, S, mp.is_solutionOptimal, T


def test():
    pb = prob.ex_h4()
    #pb = [25, 29, 33], [140, 140, 140], [0, 30, 7, 1, 30, 11, 33, 3, 28, 21], [30, 28, 5, 5, 11, 0, 18, 31, 19, 24]
    use_VC = True
    
    makespan, S, optimal, T = solve(*pb, None, 1000, use_VC=use_VC, verbose=True)
    
    print('#' * 80, '\n', makespan, optimal)
    for i, Si in enumerate(S):
        print(i, Si)
    print('#' * 80)
    for i, Ti in enumerate(T):
        print(i, Ti)


if __name__ == '__main__':
    test()
