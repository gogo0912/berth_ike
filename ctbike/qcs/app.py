from time import time

import numpy as np

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt, QPointF, QRectF

import _qapp

from ctbike.qcs import solve as qcs_solve, prob
EPSILON = prob.EPSILON


VERBOSE = True

# limit, m, n, R, D, PD, PL
DEFAULT_RANDOM_SETTING = prob.EX_GEN_AB_4_15

ALL_QCIs = True

DEFAULT_COMP_TIME_LIMIT = '10'

T_ALIGN_C = Qt.AlignHCenter | Qt.AlignVCenter

PATTERNS = [
    ('HW', 'Hatch-wise'),
    ('HW3', 'Hatch-wise3'),
    ('HW2', 'Hatch-wise2'),
    ('HW1', 'Hatch-wise1'),
    ('DF', 'Discharge-first'),
    ('DF3', 'Discharge-first3'),
    ('DF1', 'Discharge-first1'),
]


class QCSApp(QtWidgets.QMainWindow):
    
    def __init__(self, pb0, H=None, **kwds):
        super().__init__(windowTitle='QCS', **kwds)
        
        self.setCentralWidget(Schedule())
        
        self.random_settings = DEFAULT_RANDOM_SETTING
        self.cover_range = H or [[0, len(pb0[2]) - 1]] * len(pb0[0])
        
        self.ctime_limit = QtWidgets.QLineEdit(DEFAULT_COMP_TIME_LIMIT); \
            self.ctime_limit.setFixedWidth(40)
        self.all_QCIs = QtWidgets.QCheckBox('All', checked=ALL_QCIs)
        self.pattern = QtWidgets.QComboBox()
        for t, d in PATTERNS:
            self.pattern.addItem(t, d)
        self.pb_desc = QtWidgets.QLineEdit(str(pb0)[1:-1])
        
        # Tool bar
        tb = self.addToolBar('ToolBar')
        a = tb.addAction('Random'); a.setShortcut(QtGui.QKeySequence('Ctrl+R')); \
            a.triggered.connect(self.on_random)
        a = tb.addAction('Setting..'); \
            a.triggered.connect(self.on_param)
        tb.addSeparator()
        tb.addWidget(self.ctime_limit)
        #tb.addSeparator(); a = tb.addAction('T'); a.triggered.connect(self.on_test)
        tb.addSeparator()
        a = tb.addAction('Solve'); a.setShortcut(QtGui.QKeySequence('Ctrl+E')); \
            a.triggered.connect(self.solve)
        tb.addWidget(self.all_QCIs)
        a = tb.addAction('Cover'); a.triggered.connect(self.on_cover)
        tb.addWidget(self.pattern)
        tb.addWidget(self.pb_desc)
        
        QtCore.QTimer.singleShot(10, self.solve)
    
    @QtCore.pyqtSlot()
    def solve(self):
        with _qapp.hourglass():
            pb = eval(self.pb_desc.text())
            ctime_lim = eval(self.ctime_limit.text())
            
            title = 'QCS -- Limit {:,}'.format(max(pb[1]))
            self.setWindowTitle(title)
            
            pattern = self.pattern.currentData()
            QCIs = None if self.all_QCIs.isChecked() else [[*range(len(pb[0]))]]
            
            t0 = time()
            R = qcs_solve(pattern, *pb, self.cover_range, QCIs, ctime_lim, VERBOSE)
            self.setWindowTitle(title + ' (%.1f sec)' % (time() - t0))
            
            self.centralWidget().set(pb, R)
    
    @QtCore.pyqtSlot()
    def on_random(self):
        pb = prob.generate(*self.random_settings)
        self.pb_desc.setText(str(pb)[1:-1])
        self.cover_range = [[0, len(pb[2]) - 1]] * len(pb[0])
        self.solve()
    
    @QtCore.pyqtSlot()
    def on_param(self):
        dlg = QtWidgets.QInputDialog(self, windowTitle='Random Instance Parameters',
                                     size=QtCore.QSize(400, -1))
        dlg.setLabelText('limit, m, n, R, D, PD, PL')
        dlg.setTextValue(str(self.random_settings)[1:-1])
        if dlg.exec_():
            self.random_settings = eval(dlg.textValue())
    
    @QtCore.pyqtSlot()
    def on_cover(self):
        dlg = QtWidgets.QInputDialog(self, windowTitle='QC Cover Ranges',
                                     size=QtCore.QSize(400, -1))
        dlg.setLabelText('[start, end (inclusive)], ...')
        dlg.setTextValue(str(self.cover_range)[1:-1])
        if dlg.exec_():
            self.cover_range = eval(dlg.textValue())
    
    @QtCore.pyqtSlot()
    def on_test(self):
        print('hello')


class Schedule(QtWidgets.QTabWidget):
    
    def __init__(self):
        super().__init__(focusPolicy=Qt.StrongFocus)
    
    def set(self, pb, R):
        self.clear()
        
        if len(R) > 0:
            max_ms = max(r[1] for r in R)
            for QCI, ms, S, optimal, T in R:
                title = '{} {:,.1f}{}'.format(np.array(QCI), ms, '' if optimal else ' (--)')
                i = self.addTab(Pane(pb, QCI, ms, S, optimal, T, max_ms), title)
                self.setCurrentIndex(i)


MG, MG_I, VESSEL_H = 20, 10, 150  # margin, internal margin, vessel height
MG_MG_I_1, MG_MG_I_15, MG_I2 = MG + MG_I, MG + MG_I * 3 // 2, 2 * MG_I
P_H = VESSEL_H - 4 * MG_I  # max processing time height

LT_RED, LT_BLUE = [QtGui.QColor(c).lighter(180) for c in (Qt.red, Qt.blue)]
LT_GRAY = QtGui.QColor('#e0e0e0')

PEN_READY, PEN_DEAD = [QtGui.QPen(QtGui.QBrush(c), 7) for c in (Qt.cyan, Qt.magenta)]


class Pane(QtWidgets.QWidget):
    
    def __init__(self, pb, QCI, ms, S, optimal, T, max_ms):
        super().__init__()
        
        self.setBackgroundRole(QtGui.QPalette.Base)
        self.setAutoFillBackground(True)
        
        self.pb, self.QCI, self.ms, self.S, self.T, self.max_ms = pb, QCI, ms, S, T, max_ms
        
        print('QC %s with makespan %.1f' % (QCI, ms), '*' if optimal else '(--)')
        for i, Si in zip(QCI, S):
            print('  Q%d: %s' % (i, ', '.join('H%d(%s) %.1f-%.1f(%.1f)' %
                                 (h, t, s, s + p, p) for h, t, s, p in Si)))
        if T is not None:
            for i, Ti in zip(QCI, T):
                print('  Q%d: %s' % (i, ', '.join('H%d %.1f' % (j, t) for j, t in Ti)))
    
    def paintEvent(self, _e):
        qp = QtGui.QPainter(self)
        
        (r, d, pD, pL), max_ms = self.pb, self.max_ms
        n = len(pD)
        
        w_widget, h_widget = self.width(), self.height()
        w_c = w_widget - 2 * MG  # content width
        w_hc, w_hc_2 = w_c / n, w_c / n / 2  # hatch width
        scale = (h_widget - 2 * MG - MG_I - VESSEL_H) / max_ms
        
        #
        # Vessel
        
        pmax = max(max(pD), max(pL)) * 1.05
        
        y0, w = MG + 2 * MG_I, w_hc_2 - 1.5 * MG_I
        w1 = w + MG_I2
        
        qp.fillRect(QRectF(MG, MG, w_c, MG_I2), Qt.darkYellow)
        qp.fillRect(QRectF(MG, y0, w_c, VESSEL_H - MG_I2), Qt.lightGray)
        
        for j, (pDj, pLj) in enumerate(zip(pD, pL)):
            x = MG + j * w_hc
            xD, xL = x + MG_I, x + MG_I + w_hc_2 - 0.5 * MG_I
            
            pDsj, pLsj = P_H * pDj / pmax, P_H * pLj / pmax
            qp.fillRect(QRectF(xD, y0 + P_H - pDsj, w, pDsj), Qt.darkBlue)
            qp.fillRect(QRectF(xL, y0 + P_H - pLsj, w, pLsj), Qt.darkRed)
            
            qp.setPen(Qt.white)
            qp.drawText(QRectF(x, y0 - MG_I2, w_hc, MG_I2), T_ALIGN_C, 'H' + str(j))
            
            qp.setPen(Qt.black)
            qp.drawText(QRectF(xD - MG_I, y0 + P_H, w1, MG_I2), T_ALIGN_C, '{:,}'.format(pDj))
            qp.drawText(QRectF(xL - MG_I, y0 + P_H, w1, MG_I2), T_ALIGN_C, '{:,}'.format(pLj))
        
        #
        # Schedule
        
        y0, w_job = MG + VESSEL_H + MG_I, w_hc - MG_I2
        
        # Trajectory
        if self.T is not None:
            for i, Ti in zip(self.QCI, self.T):
                if len(Ti) == 0:
                    continue
                
                j0, t0 = Ti[0]
                for j1, t1 in Ti[1:]:
                    x = MG_MG_I_1 + j0 * w_hc
                    y, h = y0 + scale * t0, scale * (t1 - t0)
                    
                    qp.setPen(LT_GRAY)
                    qp.fillRect(QRectF(x, y, w_job, h), LT_GRAY)
                    qp.drawRect(QRectF(x, y, w_job, h))
                    if j1 < n: # connecting line
                        x1, y1 = MG + (j0 + 0.5) * w_hc, y + h
                        qp.drawLine(QPointF(MG + (j1 + 0.5) * w_hc, y1), QPointF(x1, y1))
                    
                    if h > 12:
                        qp.setPen(Qt.black)
                        qp.drawText(QRectF(x, y, w_job, h), T_ALIGN_C, 'Q' + str(i))
                    
                    j0, t0 = j1, t1
        
        # Ready time and due date
        for i, Si in zip(self.QCI, self.S):
            if len(Si) > 0:
                if r[i] > EPSILON:
                    qp.setPen(PEN_READY)
                    x, y = MG_MG_I_1 + Si[0][0] * w_hc - 3, y0 + scale * r[i]
                    qp.drawLine(QPointF(x, y), QPointF(x + w_job + 5, y))
                if d[i] < self.ms - EPSILON:
                    qp.setPen(PEN_DEAD)
                    x, y = MG_MG_I_1 + Si[-1][0] * w_hc - 3, y0 + scale * d[i]
                    qp.drawLine(QPointF(x, y), QPointF(x + w_job + 5, y))
        
        # Job-connecting lines (for old solutions)
        if self.T is None:
            qp.setPen(LT_GRAY)
            for Si in self.S:
                if len(Si) > 0:
                    j0, _, s0, p0 = Si[0]
                    for j, _, s, p in Si[1:]:
                        if j != j0:
                            x, y = MG + (j0 + 0.5) * w_hc, y0 + scale * s
                            if  s > s0 + p0 + EPSILON:
                                qp.drawLine(QPointF(x, y0 + scale * (s0 + p0)), QPointF(x, y))
                            qp.drawLine(QPointF(x, y), QPointF(MG + (j + 0.5) * w_hc, y))
                        j0, s0, p0 = j, s, p
        
        # Job-handling
        qp.setPen(Qt.SolidLine)
        for Si in self.S:
            if len(Si) > 0:
                for j, t, s, p in Si:
                    x = MG_MG_I_1 + j * w_hc
                    y, h = y0 + scale * s, scale * p
                    qp.fillRect(QRectF(x, y, w_job, h), LT_BLUE if t == 'D' else LT_RED)
                    qp.drawRect(QRectF(x, y, w_job, h))
        
        # Texts
        for i, Si in zip(self.QCI, self.S):
            for j, t, s, p in Si:
                x = MG + j * w_hc
                y, h = y0 + scale * s, scale * p
                pt = 'Q{}: {:,.1f}'.format(i, p)
                qp.drawText(QRectF(x, y - MG_I, w_hc, h + MG_I2), T_ALIGN_C, pt)
        
        # Makespan
        y = y0 + scale * self.ms
        qp.setPen(Qt.SolidLine)
        qp.drawLine(QPointF(MG, y0), QPointF(w_widget - MG, y0))
        qp.drawLine(QPointF(MG, y), QPointF(w_widget - MG, y))
        qp.drawText(QPointF(MG, y + 15), '{:,.1f}'.format(self.ms))


if __name__ == '__main__':
    #import random; random.seed(1)
    
    #pb0 = prob.ex_h1()
    #pb0 = prob.ex_test()
    pb0 = prob.ex_clt1()
    #pb0 = [0, 0, 8, 0], [160, 160, 160, 126], [5, 11, 26, 10, 4, 0, 24, 24, 19, 8, 0, 28, 23, 10, 6], [36, 22, 26, 0, 39, 21, 26, 0, 19, 9, 32, 39, 29, 40, 19]
    
    H = None
    #H = [[0, 3], [0, 6], [2, 12]]
    #H = [[0, 2], [0, 5]]
    
    app = QCSApp(pb0, H, size=QtCore.QSize(800, 600))
    app.show()
    
    _qapp.exec_(app)
