"""
Discharge-first
"""

import numpy as np

from mipcl_py.mipshell.mipshell import Problem, minimize, Var, sum_, BIN, REAL

from ctbike.qcs import prob
EPSILON = prob.EPSILON 


PRINT_MIP_FORMULATION = 0
PRINT_LP_FORMULATION = 0


def solve(r, d, pD, pL, H=None, ctime_limit=10, use_VC=True, verbose=False):
    """
    W[i]: work range of QC i
    """
    m, n = len(r), len(pD)
    
    # Revise so that minimum ready time is 0.
    r_min, r, d = prob.revise_to_zero(r, d)
    
    # Solve MIP.
    makespan, xD, yD, xL, yL, optimal = solve_MIP(r, d, pD, pL, H, ctime_limit, use_VC, verbose)
    
    if makespan is None:
        return [None] * 4
    
    W = [prob.get_working_range(m, n, yD), prob.get_working_range(m, n, yL)]
    if verbose:
        print('    WD =', W[0])
        print('    WL =', W[1])
    
    #
    # Improve solution
    
    for t in [0, 1]:
        I = [i for i in range(m) if W[t][i] is not None]
        
        # Check if there is no QC handling (no discharge or loading job)
        if not I:
            continue
        
        k0, k1 = 0, len(I) - 1
        i0, i1 = I[k0], I[k1]
        
        # Step 1: Make leftmost QC start from a hatch as left as possible.
        while W[t][i0][0] > 0:
            xD, yD, xL, yL = change_work_range('S1', t, i0, 0, -1, r, d, pD, pL, H, W, makespan, verbose)
        
        # Step 2: Make rightmost QC finish at a hatch as right as possible.
        while W[t][i1][1] < n - 1:
            xD, yD, xL, yL = change_work_range('S2', t, i1, 1, 1, r, d, pD, pL, H, W, makespan, verbose)
        
        while k0 < k1:
            # Step 3
            while W[t][i0][1] > W[t][i0][0]:
                xy1 = change_work_range('S3', t, i0, 1, -1, r, d, pD, pL, H, W, makespan, verbose)
                if xy1 is None:
                    break
                xD, yD, xL, yL = xy1
            
            # Step 4
            while W[t][i1][0] < W[t][i1][1]:
                xy1 = change_work_range('S4', t, i1, 0, 1, r, d, pD, pL, H, W, makespan, verbose)
                if xy1 is None:
                    break
                xD, yD, xL, yL = xy1
            
            # Move to next.
            k0, k1 = k0 + 1, k1 - 1
            if k1 < k0:
                break
            i0, i1 = I[k0], I[k1]
            
            # Step 5
            while W[t][i0][0] < W[t][i0][1]:
                xy1 = change_work_range('S5', t, i0, 0, 1, r, d, pD, pL, H, W, makespan, verbose)
                if xy1 is None:
                    break
                xD, yD, xL, yL = xy1
            
            # Step 6
            while W[t][i1][1] > W[t][i0][0]:
                xy1 = change_work_range('S6', t, i1, 1, -1, r, d, pD, pL, H, W, makespan, verbose)
                if xy1 is None:
                    break
                xD, yD, xL, yL = xy1
    
    #
    # Solution
    
    S, _pD, _pL = [[] for _ in range(m)], list(pD), list(pL)
    
    for i in reversed(range(m)):  # reverse because right QC handles first
        for j in range(n):
            if yD[i][j] > EPSILON and _pD[j] > EPSILON:
                xd = min(yD[i][j], _pD[j])
                S[i].append([j, 'D', max(xD[i][j], r[i]), xd])
                _pD[j] -= xd
    
    for i in range(m):
        for j in reversed(range(n)):
            if yL[i][j] > EPSILON and _pL[j] > EPSILON:
                xl = min(yL[i][j], _pL[j])
                s = max(max(xD[i][j], r[i]) + yD[i][j], xL[i][j + 1], r[i])
                S[i].append([j, 'L', s, xl])
                _pL[j] -= xl
    
    T = [[] for _ in range(m)]
    
    for i in range(m):
        if W[0][i] is None or W[1][i] is None:
            continue
        
        for jD in range(n + 1):
            if jD == n or jD > max(W[0][i][1], W[1][i][1]):
                break
            T[i].append([jD, xD[i][jD]])
        if jD > 0:
            T[i].append([jD - 1, xL[i][jD - 1]])  # This is for correct drawing.
            for jL in reversed(range(jD)):
                T[i].append([jL - 1, xL[i][jL]])
    
    # Recover solution from r_min-revision, if necessary.
    if r_min > 0:
        makespan, S, T = prob.revise_to_r_min(r_min, makespan, S, T)
    
    return makespan, S, optimal, T


def change_work_range(lbl, t, i, k, q, r, d, pD, pL, H, W, makespan, verbose):
    """
    t: type (0: discharge, 1: loading)
    """
    W[t][i][k] += q
    if verbose:
        print('%s: W%s = %s' % (lbl, ['D', 'L'][t], W[t]), end='')
    
    xy = solve_LP(r, d, pD, pL, H, W, makespan)
    if xy is not None:
        if verbose:
            print()
        return xy
    
    W[t][i][k] -= q
    if verbose:
        print(' X')
    return None


def solve_MIP(r, d, pD, pL, H, ctime_limit, use_VC, verbose):
    """
    A[i], B[i]: whether QC i is not available from beginning and to end, respectively
    
    Decision variables (T as D or L)
      xD
      ...
    
    RETURN makespan, schedule S, optimality
      S[k]: [hatch, 'D' or 'L', start time, processing time]
    """
    m, n = len(r), len(pD)
    d_max, avail = max(d), [d[i] - r[i] for i in range(m)]
    
    assert n >= 2 and np.less(r, d).all()
    
    A = [r[i] > EPSILON for i in range(m)]
    B = [d[i] < d_max - EPSILON for i in range(m)]
    
    pD1 = [[min(pD[j], avail[i]) for j in range(n)] for i in range(m)]
    pL1 = [[min(pL[j], avail[i]) for j in range(n)] for i in range(m)]
    
    #
    # Model and variables
    
    mp = Problem('Discharge-first')
    
    xD = [[Var('xD(%d,%d)' % (i, j), REAL, 0, d_max) for j in range(n + 1)]
         for i in range(m)]
    xL = [[Var('xL(%d,%d)' % (i, j), REAL, 0, d_max) for j in range(n + 1)]
         for i in range(m)]
    yD = [[Var('yD(%d,%d)' % (i, j), REAL, 0, pD1[i][j]) for j in range(n)]
         for i in range(m)]
    yL = [[Var('yL(%d,%d)' % (i, j), REAL, 0, pL1[i][j]) for j in range(n)]
         for i in range(m)]
    uD = [[Var('uD(%d,%d)' % (i, j), BIN) for j in range(n)] if A[i] else None
         for i in range(m)]
    uL = [[Var('uL(%d,%d)' % (i, j), BIN) for j in range(n)] if A[i] else None
         for i in range(m)]
    vD = [[Var('vD(%d,%d)' % (i, j), BIN) for j in range(n)] if B[i] else None
         for i in range(m)]
    vL = [[Var('vL(%d,%d)' % (i, j), BIN) for j in range(n)] if B[i] else None
         for i in range(m)]
    w = [[Var('w(%d,%d)' % (i, j), BIN) for j in range(n)] for i in range(m)]
    
    #
    # Objective
    
    minimize(xL[m - 1][0])
    
    #
    # Constraints
    
    for i in range(m):
        for j in range(n):
            _C = yD[i][j] <= xD[i][j + 1] - xD[i][j]
            _C = yL[i][j] <= xL[i][j] - xL[i][j + 1]
        
        if A[i]:
            for j in range(n):
                _C = yD[i][j] <= pD1[i][j] * (1 - uD[i][j])
                _C = yD[i][j] <= xD[i][j + 1] - r[i] + r[i] * uD[i][j]
                
                _C = yL[i][j] <= pL1[i][j] * (1 - uL[i][j])
                _C = yL[i][j] <= xL[i][j] - r[i] + r[i] * uL[i][j]
        
        if B[i]:
            for j in range(n):
                _C = yD[i][j] <= pD1[i][j] * vD[i][j]
                _C = yD[i][j] <= d[i] - xD[i][j] + (d_max - d[i]) * (1 - vD[i][j])
                
                _C = yL[i][j] <= pL1[i][j] * vL[i][j]
                _C = yL[i][j] <= d[i] - xL[i][j + 1] + (d_max - d[i]) * (1 - vL[i][j])
    
    for i in range(m - 1):
        for j in range(n - 1):
            _C = xD[i][j] >= xD[i + 1][j + 2]
            _C = xL[i][j] <= xL[i + 1][j + 2]
    
    for j in range(n):
        _C = sum_(yD[i][j] for i in range(m)) >= pD[j]
        _C = sum_(yL[i][j] for i in range(m)) >= pL[j]
    
    # Connect discharge and loading.
    for i in range(m):
        for j in range(n):
            _C = yD[i][j] + yL[i][j] <= xL[i][j] - xD[i][j] + d_max * (1 - w[i][j])
            _C = yD[i][j] + yL[i][j] <= min(pD1[i][j] + pL1[i][j], avail[i])
            
            _C = yD[i][j] <= pD1[i][j] * w[i][j]
            _C = yL[i][j] <= pL1[i][j] * w[i][j]
        
        if A[i]:
            for j in range(n):
                _C = yD[i][j] + yL[i][j] <= xL[i][j] - r[i] + r[i] * uL[i][j]
                _C = yD[i][j] <= pD1[i][j] * (1 - uL[i][j])  # strange but indispensable
        
        if B[i]:
            for j in range(n):
                _C = yD[i][j] + yL[i][j] <= d[i] - xD[i][j] + (d_max - d[i]) * (1 - vD[i][j])
                _C = yL[i][j] <= pL1[i][j] * vD[i][j]  # strange but indispensable
    
    # Cover range
    if H is not None:
        for i, (h0j, h1j) in enumerate(H):
            assert h0j <= h1j
            for j in range(0, h0j):
                    _C = yD[i][j] == 0
                    _C = yL[i][j] == 0
            for j in range(h1j + 1, n):
                    _C = yD[i][j] == 0
                    _C = yL[i][j] == 0
    
    #
    # Valid inequalities
    
    if use_VC:
        for i in range(m):
            if A[i]:
                for j in range(n - 1):
                    _C = uD[i][j] >= uD[i][j + 1]
                    _C = uL[i][j + 1] >= uL[i][j]
                
                for j in range(n):
                    _C = xD[i][j + 1] <= r[i] + (d_max - r[i]) * (1 - uD[i][j])
                    _C = xL[i][j] <= r[i] + (d_max - r[i]) * (1 - uL[i][j])
            
            if B[i]:
                for j in range(n - 1):
                    _C = vD[i][j] >= vD[i][j + 1]
                    _C = vL[i][j + 1] >= vL[i][j]
                    
                for j in range(n):
                    _C = xD[i][j] <= d[i] + (d_max - d[i]) * (1 - vD[i][j])
                    _C = xL[i][j + 1] <= d[i] + (d_max - d[i]) * (1 - vL[i][j])
            
        
        for i in range(m):
            for j in range(n - 1):
                _C = w[i][j] >= w[i][j + 1]
        
        for i in range(m - 1):
            for j in range(n - 2):
                _C = w[i + 1][j + 2] >= w[i][j]
    
    # Invalid inequalities
    '''
    for i in range(m):
        if A[i]:
            for j in range(n - 1):
                # xD[k..n] and xL[l..n] should float freely.
                _C = uD[i][n - 1] >= uL[i][n - 1]
        if B[i]:
            for j in range(n - 1):
                # With same reason above.
                _C = vL[i][n - 1] >= vD[i][n - 1]
    '''
    
    if verbose and PRINT_MIP_FORMULATION:
        mp._print()
    
    #
    # Solve!
    
    mp.optimize(not verbose, ctime_limit)
    
    if not mp.is_solution:
        return [None] * 6
    
    #
    # Solution
    
    makespan = xL[m - 1][0].val
    xD, yD = [[xij.val for xij in xi] for xi in xD], [[yij.val for yij in yi] for yi in yD]
    xL, yL = [[xij.val for xij in xi] for xi in xL], [[yij.val for yij in yi] for yi in yL]
    
    #
    #
    #
    # TODO!!!!!!!!!!!!!!!!
    if not np.allclose(np.sum(yD, 0), pD) or not np.allclose(np.sum(yL, 0), pL):
        return [None] * 6
    #
    #
    #
    
    return makespan, xD, yD, xL, yL, mp.is_solutionOptimal


def solve_LP(r, d, pD, pL, H, W, makespan):
    m, n = len(r), len(pD)
    d_max, avail = max(d), [d[i] - r[i] for i in range(m)]
    
    pD1 = [[min(pD[j], avail[i]) for j in range(n)] for i in range(m)]
    pL1 = [[min(pL[j], avail[i]) for j in range(n)] for i in range(m)]
    
    #
    # Model and variables
    
    mp = Problem('Discharge-first-LP')
    
    xD = [[Var('xD(%d,%d)' % (i, j), REAL, 0, d_max) for j in range(n + 1)]
          for i in range(m)]
    xL = [[Var('xL(%d,%d)' % (i, j), REAL, 0, d_max) for j in range(n + 1)]
          for i in range(m)]
    yD = [[Var('yD(%d,%d)' % (i, j), REAL, 0, pD1[i][j]) for j in range(n)]
          for i in range(m)]
    yL = [[Var('yL(%d,%d)' % (i, j), REAL, 0, pL1[i][j]) for j in range(n)]
          for i in range(m)]
    
    #
    # Objective
    
    minimize(xL[m - 1][0])
    
    #
    # Constraints
    
    WD, WL = W
    
    for i in range(m):
        if WD[i] is not None:
            wDi0, wDi1 = WD[i]
            
            for j in range(wDi0):
                _C = xD[i][j + 1] >= xD[i][j]
                _C = yD[i][j] == 0
            
            _C = yD[i][wDi0] <= xD[i][wDi0 + 1] - r[i]
            for j in range(wDi0, wDi1 + 1):
                _C = yD[i][j] <= xD[i][j + 1] - xD[i][j]
            _C = yD[i][wDi1] <= d[i] - xD[i][wDi1]
            
            for j in range(wDi1 + 1, n):
                _C = xD[i][j + 1] >= xD[i][j]
                _C = yD[i][j] == 0
        
        else:
            for j in range(n):
                _C = xD[i][j + 1] >= xD[i][j]
                _C = yD[i][j] == 0
        
        if WL[i] is not None:
            wLi0, wLi1 = WL[i]
            
            for j in range(wLi1 + 1, n):
                _C = xL[i][j] >= xL[i][j + 1]
                _C = yL[i][j] == 0
            
            _C = yL[i][wLi1] <= xL[i][wLi1] - r[i]
            for j in range(wLi0, wLi1 + 1):
                _C = yL[i][j] <= xL[i][j] - xL[i][j + 1]
            _C = yL[i][wLi0] <= d[i] - xL[i][wLi0 + 1]
            
            for j in range(wLi0):
                _C = xL[i][j] >= xL[i][j + 1]
                _C = yL[i][j] == 0
        
        else:
            for j in range(n):
                _C = xL[i][j] >= xL[i][j + 1]
                _C = yL[i][j] == 0
        
    for i in range(m - 1):
        for j in range(n - 1):
            _C = xD[i][j] >= xD[i + 1][j + 2]
            _C = xL[i][j] <= xL[i + 1][j + 2]
    
    for j in range(n):
        _C = sum_(yD[i][j] for i in range(m)) >= pD[j]
        _C = sum_(yL[i][j] for i in range(m)) >= pL[j]
    
    # Connect discharge and loading.
    for i in range(m):
        if WD[i] is None and WL[i] is None:
            continue
        
        j = max(-1 if WD[i] is None else WD[i][1], -1 if WL[i] is None else WL[i][1])
        _C = yD[i][j] + yL[i][j] <= xL[i][j] - xD[i][j]
    
    # Cover range
    if H is not None:
        for i, (h0j, h1j) in enumerate(H):
            for j in range(n):
                if j < h0j or j > h1j:
                    _C = yD[i][j] == 0
                    _C = yL[i][j] == 0
    
    _C = xL[m - 1][0] <= makespan + EPSILON
    
    #
    # Solve!
    
    mp.optimize()
    
    if not mp.is_solution:
        return None
    
    #
    # Solution
    
    xD, yD = [[xij.val for xij in xi] for xi in xD], [[yij.val for yij in yi] for yi in yD]
    xL, yL = [[xij.val for xij in xi] for xi in xL], [[yij.val for yij in yi] for yi in yL]
    assert np.isclose(makespan, xL[m - 1][0]), (makespan, xL[m - 1][0])
    
    return xD, yD, xL, yL


def test():
    #pb = prob.ex_test()
    pb = [0, 0, 47], [140, 140, 140], [12, 15, 25, 21, 10, 8, 0, 25, 21, 12], [31, 8, 22, 0, 4, 6, 0, 9, 35, 11]
    use_VC = True
    
    makespan, S, optimal, T = solve(*pb, None, 1000, use_VC, verbose=True)
    
    print('#' * 80, '\n', makespan, optimal)
    for Si in S:
        print(Si)
    print('#' * 80)
    for i, Ti in enumerate(T):
        print(i, Ti)


if __name__ == '__main__':
    test()
